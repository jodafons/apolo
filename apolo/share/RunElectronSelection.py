
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
from RecExConfig.RecFlags import rec
from RecExConfig.RecAlgsFlags import recAlgs
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags as acf
from apolo import get_files
from glob import glob
import os


if 'args' in dir():
  print(args)
else:
  raise RuntimeError("args dict not found")

tags = args['tags']
trigList = args['triggers']

#f (args['files']) is str:
#  finallist = get_files(args['files'])
#else:
finallist = args['files']
sel = args['selection']
nov = args['nov']

massRange = args['massRange']
grllist = args['grllist']
runFlag = args['runFlag']


####################################################################################################

DetailedHistograms=False
acf.FilesInput=finallist
acf.EvtMax=nov
rec.readAOD=True
# switch off detectors
rec.doForwardDet=False
rec.doInDet=False
rec.doCalo=False
rec.doMuon=False
rec.doEgamma=False
rec.doTrigger=True
recAlgs.doTrigger=False # disable trigger (maybe necessary if detectors switched off)
rec.doMuon=False
rec.doMuonCombined=False
rec.doWriteAOD=True
rec.doWriteESD=False
rec.doDPD=False
rec.doTruth=False
# autoconfiguration might trigger undesired feature
rec.doESD.set_Value_and_Lock(False)      # uncomment if do not run ESD making algorithms
rec.doWriteESD.set_Value_and_Lock(False) # uncomment if do not write ESD
rec.doAOD.set_Value_and_Lock(True)       # uncomment if do not run AOD making algorithms
rec.doWriteAOD.set_Value_and_Lock(False) # uncomment if do not write AOD
rec.doWriteTAG.set_Value_and_Lock(False) # uncomment if do not write TAG

# main jobOption
include ("RecExCommon/RecExCommon_topOptions.py")
MessageSvc.debugLimit = 20000000
MessageSvc.infoLimit  = 20000000

# Add Trigger Decision Tool
from TrigDecisionTool.TrigDecisionToolConf import Trig__TrigDecisionTool
tdt = Trig__TrigDecisionTool( "TrigDecisionTool" )
tdt.TrigDecisionKey='xTrigDecision'
tdt.Navigation.ReadonlyHolders=True
ToolSvc+=tdt

try:
  include ("AthenaMonitoring/DataQualityInit_jobOptions.py")
except Exception:
  treatException("Could not load AthenaMonitoring/DataQualityInit_jobOptions.py")

# Call top sequence
from AthenaCommon.AlgSequence import AlgSequence, AthSequencer
topSequence = AlgSequence()


##################################### GRL Tools ##########################################
# Good Run List (GRL)
if len(grllist)>0:
  # update and fix the path
  from apolo import find_calib_path
  for idx, grl in enumerate(grllist):
    grllist[idx] = find_calib_path(grl)

  from RecExConfig.InputFilePeeker import inputFileSummary
  try:
    if inputFileSummary['evt_type'][0] == "IS_DATA":
      from GoodRunsLists.GoodRunsListsConf import *
      ToolSvc += GoodRunsListSelectorTool()
      GoodRunsListSelectorTool.GoodRunsListVec = grllist
      from GoodRunsListsUser.GoodRunsListsUserConf import *
      seq = AthSequencer("AthFilterSeq")
      seq += GRLTriggerSelectorAlg('GRLTriggerAlg1')
      seq.GRLTriggerAlg1.GoodRunsListArray = ['PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim']
      #seq.GRLTriggerAlg1.GoodRunsListArray = [grllist[0].replace('.xml','')]
  except:
    print ("GRL not available!")

##################################### Mon Tools ##########################################


from AthenaMonitoring.AthenaMonitoringConf import AthenaMonManager
topSequence += AthenaMonManager( "HLTMonManager")
HLTMonManager = topSequence.HLTMonManager
#Global HLTMonTool
from TrigHLTMonitoring.TrigHLTMonitoringConf import HLTMonTool
HLTMon = HLTMonTool(name  = 'HLTMon', histoPathBase = "HLT")
ToolSvc += HLTMon
HLTMonManager.AthenaMonTools += [ "HLTMonTool/HLTMon" ]

from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConfig import TrigEgammaPlotTool
PlotTool = TrigEgammaPlotTool.copy( name="TrigEgammaPlotTool",
                                    DirectoryPath='/HLT/Egamma',
                                    MaM=[],
                                    Efficiency=[],
                                    Distribution=[],
                                    Resolution=[])


####################################################################################################
from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConfig import getElectronSelectionTool
TrigEgammaEventSelection = getElectronSelectionTool( runFlag )
from TrigEgammaAnalysisTools.TrigEgammaProbelist import supportTriggerList, monitoringTP_electron
supportTriggerList+=monitoringTP_electron




MinMass = massRange[0]
MaxMass = massRange[1]

# Create event selection tool
Tool = TrigEgammaEventSelection( name='EventSelection',
                              PlotTool                  = PlotTool,
                              OutputLevel               = 1,
                              ZeeLowerMass              = MinMass,
                              ZeeUpperMass              = MaxMass,
                              OfflineTagSelector        = 'LHTight', # 1=tight, 2=medium, 3=loose
                              OfflineProbeSelector      = 'LHLoose',
                              OfflineTagMinEt           = 4.5 if sel=='jpsiee' else 26.0,
                              OfflineProbeMinEt         = 4.5 if sel=='jpsiee' else 4.0,
                              TagTriggerList            = args['tags'],
                              SelectionZ                = True if sel=='zee' else False,
                              SelectionMC               = args['mc'],
                              SelectionJpsi             = True if sel=='jpsiee' else False,
                              SelectionFakes            = True if sel=='fakes' else False,
                              ApplyMinimalTrigger       = True,
                              TriggerList               = args['triggers'],
                              SupportTriggerList        = supportTriggerList,
                              # Jpsi special cuts
                              DoJpsiTagTrackIsolation   = True,
                              DoJpsiTagTighterTRT       = False,
                              DoJpsiProbeTRTCut         = False,
                              DoJpsiProbeSiCut          = True,
                              JpsiTriggers              = args['tags'] , # shold be jpsiee in the name
                              JpsiDeltaR                = args['deltaR'],
                              isBphys                   = args['isBphys']
                              )

####################################################################################################
Tools=['TrigEgammaElectronSelection/EventSelection']
from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConf import TrigEgammaMonTool
TrigEgammaMonTool = TrigEgammaMonTool( name = "HLTEgammaMon", histoPathBase='/HLT/Egamma', Tools=Tools)
ToolSvc += TrigEgammaMonTool
HLTMonManager.AthenaMonTools += [ "TrigEgammaMonTool/HLTEgammaMon" ]
HLTMonManager.FileKey = "GLOBAL"
ToolSvc.TrigDecisionTool.Navigation.OutputLevel = WARNING
####################################################################################################