#!/usr/bin/env python

from pprint import pprint
import argparse
from AthenaCommon.Logging import logging
logger = logging.getLogger( 'RunEventSelectionParser' )

parser = argparse.ArgumentParser(description = '', add_help = False)
parser = argparse.ArgumentParser()

parser.add_argument('-n','--nov', action='store',
    dest='nov', required = False, default = 100, type=int,
    help = "Number of events.")

parser.add_argument('--grid', action='store_true',
    dest='grid', required = False, default = False,
    help = "to launch on the grid.")

parser.add_argument('-c','--campaign', action='store',
    dest='campaign', required = False, type=str, default=None,
    help = "The name of grid campaign.")

parser.add_argument('--runFlag', action='store',
    dest='runFlag', required = False, type=str, default='2018',
    help = "Set the offline calibpaths from analysis using the runFlag (2017 OR 2018)")

parser.add_argument('-f', '--files', action='store',
    dest='files', required = False, default = False, type=str,
    help = "Path to AOD sample.")

parser.add_argument('--mc', action='store_true',
    dest='mc', required = False, default = False,
    help = "Turn on the MC flag into event selection")

parser.add_argument('--dry_run', action='store_true',
    dest='dry_run', required = False, default = False,
    help = "Do not launch if this is True.")

parser.add_argument('-t','--tag', action='store',
    dest='tag', required = False, type=str, default=None,
    help = "Tag to be append in the end of the output container (just for grid) max 5 characters")

parser.add_argument('--nEventsPerJob', action='store',
    dest='nEventsPerJob', required = False, type=int, default=4000,
    help = "Number of evetns per job in grid mode.")

parser.add_argument('--zee', action='store_true',
    dest='zee', required = False, default = False,
    help = "Run z->ee event selection.")

parser.add_argument('--fakes', action='store_true',
    dest='fakes', required = False, default = False,
    help = "Run fakes event selection.")

parser.add_argument('--jpsiee',  action='store_true',
    dest='jpsiee', required = False, default = False,
    help = "Run jpsi->ee event selection.")

parser.add_argument('--boosted',  action='store_true',
    dest='boosted', required = False, default = False,
    help = "Run Z->ee + e boosted event selection.")

parser.add_argument('--nonresonant',  action='store_true',
    dest='nonresonant', required = False, default = False,
    help = "The B decay is non resonant.")

parser.add_argument('--massRange', action='store',
    dest='massRange', required = False,  default=None,
    help = "T&P Mass range. Should be '[MinMass, MaxMass]'")

parser.add_argument('--stop_ds_name_in',  action='store',
    dest='stop_ds_name_in', required = False, default = None,
    help = "Used for large dsin name... we stop in")

parser.add_argument('--noTriggerTags',  action='store_true',
    dest='noTriggerTags', required = False, default = False,
    help = "Trigger tags list is empty. Use this flag to apply T&P offline like.")


import sys,os
if len(sys.argv)==1:
  parser.print_help()
  sys.exit(1)


def stop_in( name, part_name ):
  return name.split(part_name)[0]+part_name


args = parser.parse_args()

_args = {'files':[]}
_args['mc'] = True if args.mc else False
_args['nov'] = args.nov
_args['runFlag'] = args.runFlag
_args['deltaR']  = 0.2
_args['isBphys'] = False

if args.runFlag == '2017':
  _args['grllist'] = ['apolo/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml' ]
elif args.runFlag == '2018':
  _args['grllist'] = ['apolo/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml' ]


from apolo import monitoringTP_Zee_2018, monitoringTP_Zee_2018_tags, monitoringTP_Jpsiee_2018_tags, monitoringTP_Jpsiee_2018, \
                  monitoringTP_Zllg_2020, monitoringTP_Zllg_2020_tags


job = 'apolo/RunElectronSelection.py'


if args.zee:
  _args['selection'] = 'zee'
  _args['tags'] = monitoringTP_Zee_2018_tags
  _args['triggers'] = monitoringTP_Zee_2018
  _args['massRange'] = [80,100]
elif args.fakes:
  _args['selection'] = 'fakes'
  _args['tags'] = monitoringTP_Zee_2018_tags
  _args['triggers'] = monitoringTP_Zee_2018+monitoringTP_Jpsiee_2018
  _args['massRange'] = [80,100]
elif args.jpsiee:
  _args['isBphys'] = False
  _args['selection'] = 'jpsiee'
  _args['tags'] = monitoringTP_Jpsiee_2018_tags
  _args['triggers'] = monitoringTP_Jpsiee_2018
  if args.boosted:
    import math
    _args['massRange'] = [0, math.sqrt(6)]
    _args['deltaR']    = 0.1
    _args['isBphys'] = True
  else:
    _args['massRange'] = [2.8,3.3]
    _args['deltaR']    = 0.2
# overwrite mass interval
if args.massRange:
  _args['massRange'] = eval(args.massRange)

if args.noTriggerTags:
  _args['tags'] = [] # force to be empty. Offline tag and probe


# Mode operation
if args.grid:

  command = """pathena -c {ARGS} {JOB} \\
              --inDS={INDS} \\
              --outDS={OUTDS} \\
              --nEventsPerJob={NEVENTSPERJOB} \\
              --supStream=EXPERT,SHIFT,run_1,RUNSTAT,DEBUG \\
              --mergeOutput \\
              --extOutFile '*.xml' \\
              """
  
  from apolo import grid_mc_files, grid_pp_files
  if 'mc' in args.campaign:
    files = grid_mc_files[args.campaign]
  else:
    files = grid_pp_files[args.campaign]

  for idx, inds in enumerate(files):

    logger.info( 'Container %d/%d', idx+1,len(files))
    if ':' in inds: inds=inds.split(':')[1]
    if (len(inds) + len('user.') + len(args.tag) + len(os.environ['USER']) + 2)> 120:
      logger.warning('The size of output ds exceed the max number of characteres. Please rename it')
    
    if args.stop_ds_name_in:
      outds = stop_in( inds, args.stop_ds_name_in) + 'ID'+str(idx)
      outds = 'user.{USER}.{INDS}.{TAG}'.format(USER=os.environ['USER'],INDS=outds,TAG=args.tag)
    else:
      outds = 'user.{USER}.{INDS}.{TAG}'.format(USER=os.environ['USER'],INDS=inds,TAG=args.tag)


    logger.info('OUTDS with %i \n file: %s' %(len(outds), outds))
    __args = '"args=%s"'%str(_args)

    pprint( command.format(JOB=job, ARGS=__args, INDS=inds, OUTDS=outds, NEVENTSPERJOB=args.nEventsPerJob) )

    if not args.dry_run:
      os.system(command.format(JOB=job, ARGS=__args, INDS=inds, OUTDS=outds, NEVENTSPERJOB=args.nEventsPerJob))



else:
  sample = args.files
  _args['files'] = [sample]
  __args = '"args=%s"'%str(_args)
  command = 'athena -c {ARGS} {JOB} &> mylog.log &'.format(JOB=job, ARGS=__args)
  pprint(command)
  if not args.dry_run:
    os.system(command)