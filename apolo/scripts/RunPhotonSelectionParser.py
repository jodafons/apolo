#!/usr/bin/env python

import pprint
import argparse
from AthenaCommon.Logging import logging
logger = logging.getLogger( 'RunEventSelectionParser' )

parser = argparse.ArgumentParser(description = '', add_help = False)
parser = argparse.ArgumentParser()

parser.add_argument('-n','--nov', action='store',
    dest='nov', required = False, default = 100, type=int,
    help = "Number of events.")

parser.add_argument('--grid', action='store_true',
    dest='grid', required = False, default = False,
    help = "to launch on the grid.")

parser.add_argument('-c','--campaign', action='store',
    dest='campaign', required = False, type=str, default=None,
    help = "The name of grid campaign.")

parser.add_argument('--runFlag', action='store',
    dest='runFlag', required = False, type=str, default='2018',
    help = "Set the offline calibpaths from analysis using the runFlag (2017 OR 2018)")

parser.add_argument('--test', action='store_true',
    dest='test', required = False, default = False,
    help = "Test and egamma 1 sample.")

parser.add_argument('-f', '--files', action='store',
    dest='files', required = False, default = False,
    help = "Path to AOD sample.")

parser.add_argument('--mc', action='store_true',
    dest='mc', required = False, default = False,
    help = "Turn on the MC flag into event selection")

parser.add_argument('--dry_run', action='store_true',
    dest='dry_run', required = False, default = False,
    help = "Do not launch if this is True.")

parser.add_argument('-t','--tag', action='store',
    dest='tag', required = False, type=str, default=None,
    help = "Tag to be append in the end of the output container (just for grid) max 5 characters")

parser.add_argument('--nEventsPerJob', action='store',
    dest='nEventsPerJob', required = False, type=int, default=4000,
    help = "Number of evetns per job in grid mode.")

parser.add_argument('--photon', action='store_true',
    dest='photon', required = False, default = False,
    help = "Run photon event selection.")

parser.add_argument('--fakes', action='store_true',
    dest='fakes', required = False, default = False,
    help = "Run fakes event selection.")




import sys,os
if len(sys.argv)==1:
  parser.print_help()
  sys.exit(1)


args = parser.parse_args()

_args = {'files':[]}
_args['mc'] = True if args.mc else False
_args['nov'] = args.nov
_args['runFlag'] = args.runFlag
_args['grllist'] = []
_args['tags'] = []
_args['triggers'] = []


job = 'apolo/RunPhotonSelection.py'


if args.photon:
  _args['selection'] = 'photon'
  _args['mc'] = True
elif args.fake:
  _args['selection'] = 'fakes'



# Mode operation
if args.grid:

  command = """pathena -c {ARGS} {JOB} \\
              --inDS={INDS} \\
              --outDS={OUTDS} \\
              --nEventsPerJob={NEVENTSPERJOB} \\
              --supStream=EXPERT,SHIFT,run_1,RUNSTAT,DEBUG \\
              --mergeOutput \\
              --extOutFile '*.xml' \\
              """
  
  from apolo import grid_mc_files, grid_pp_files
  if 'mc' in args.campaign:
    files = grid_mc_files[args.campaign]
  else:
    files = grid_pp_files[args.campaign]

  for idx, inds in enumerate(files):

    logger.info( 'Container %d/%d', idx+1,len(files))
    if ':' in inds: inds=inds.split(':')[1]
    if (len(inds) + len('user.') + len(args.tag) + len(os.environ['USER']) + 2)> 120:
      logger.fatal('The size of output ds exceed the max number of characteres. Please rename it')
    
    outds = 'user.{USER}.{INDS}.{TAG}'.format(USER=os.environ['USER'],INDS=inds,TAG=args.tag)
    logger.info('OUTDS with %i \n file: %s' %(len(outds), outds))
    _args = '"args=%s"'%str(_args)
    pprint( command.format(JOB=job, ARGS=_args, INDS=inds, OUTDS=outds, NEVENTSPERJOB=args.nEventsPerJob) )

    if not args.dry_run:
      os.system(command.format(JOB=job, ARGS=_args, INDS=inds, OUTDS=outds, NEVENTSPERJOB=args.nEventsPerJob))


elif args.test:
  sample = '/eos/user/j/jodafons/cern_data/test_samples/data18_13TeV.periodC.physics_Main.PhysCont.DAOD_EGAM1.grp18_v01_p3544_p3553_p3583'
  _args['files'] = sample
  _args = '"args=%s"'%str(_args)
  command = 'athena -c {ARGS} {JOB} &> mylog.log &'.format(JOB=job, ARGS=_args)
  pprint.pprint(command)
  if not args.dry_run:
    os.system(command)

else:
  sample = args.files
  _args['files'] = sample
  _args = '"args=%s"'%str(_args)
  command = 'athena -c {ARGS} {JOB} &> mylog.log &'.format(JOB=job, ARGS=_args)
  pprint.pprint(command)
  if not args.dry_run:
    os.system(command)

















