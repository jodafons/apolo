#!/usr/bin/env python

import argparse

parser = argparse.ArgumentParser(description = '', add_help = False)
parser = argparse.ArgumentParser()


parser.add_argument('-c','--configName', action='store',
    dest='ConfigName', required = True, type=str,
    help = "The name of the dict in GridFiles.")

parser.add_argument('-t','--tag', action='store',
    dest='tag', required = False, type=str, default=None,
    help = "Tag to be append in the and of the output container (just for grid)")

parser.add_argument('--user', action='store',
    dest='user', required = False, default=None,
    help = "the user name")

parser.add_argument('--dry_run', action='store_true',
    dest='dry_run', required = False, default = False,
    help = "Do not launch if this is True.")


import sys,os
if len(sys.argv)==1:
  parser.print_help()
  sys.exit(1)


args = parser.parse_args()


from apolo import grid_pp_files, grid_mc_files
if 'mc' in args.ConfigName:
  files = grid_mc_files[args.ConfigName]
else:
  config = grid_pp_files[args.ConfigName]
  files = config['datadir']

user = args.user if args.user else os.environ['USER']
for idx, inds in enumerate(files):
  if ':' in inds: inds=inds.split(':')[1]
  if (len(inds) + \
      len('user.') + \
      len(args.tag) + \
      len(os.environ['USER']) + 2)> 120:
      print('This will cause problem... creating a hash using to avoid the problem.')
      inds = args.ConfigName+'.index%i' %(idx)
      #outds = 'user.{USER}.{INDS}.{TAG}'.format(USER=os.environ['USER'],
      #                                        INDS=n_inds,
      #                                        TAG=args.tag)

  command = 'rucio download user.{USER}.{INDS}.{TAG}_GLOBAL'.format(USER=user,
                                                                      INDS=inds,
                                                                      TAG=args.tag)
  print command

  if not args.dry_run:
    os.system(command)





