__all__  = ['grid_mc_files']

# Grid datasets used in 2017/2018 
# updated in 2020 adding two new datasets.
grid_mc_files = {

  'mc16_Zee_boosted' : [
    'mc16_13TeV.309995.MGPy8EG_A14N23LO_GGF_radion_ZZ_llqq_kl35L3_m3000.merge.AOD.e6741_e5984_s3126_r10201_r10210_tid14292735_00',
    'mc16_13TeV.309995.MGPy8EG_A14N23LO_GGF_radion_ZZ_llqq_kl35L3_m3000.merge.AOD.e6741_e5984_s3126_r9364_r9315_tid14276187_00',
    'mc16_13TeV.309995.MGPy8EG_A14N23LO_GGF_radion_ZZ_llqq_kl35L3_m3000.merge.AOD.e6741_e5984_s3126_r10724_r10726_tid16292567_00',
    #'mc16_13TeV.341330.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH3000NW_ZZllqq.merge.AOD.e3940_e5984_s3126_r10201_r10210_tid13604130_00',
    #'mc16_13TeV.341330.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH3000NW_ZZllqq.merge.AOD.e3940_e5984_s3126_r10724_r10726_tid15974962_00',
    #'mc16_13TeV.302236.MadGraphPythia8EvtGen_A14NNPDF23LO_HVT_Agv1_VcWZ_llqq_m3000.merge.AOD.e4069_e5984_s3126_r9364_r9315_tid12321668_00',
    #'mc16_13TeV.302236.MadGraphPythia8EvtGen_A14NNPDF23LO_HVT_Agv1_VcWZ_llqq_m3000.merge.AOD.e4069_e5984_s3126_r10201_r10210_tid13572586_00',
    #'mc16_13TeV.302236.MadGraphPythia8EvtGen_A14NNPDF23LO_HVT_Agv1_VcWZ_llqq_m3000.merge.AOD.e4069_e5984_s3126_r10724_r10726_tid15971406_00',
    #'mc16_13TeV.303297.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_ZZ_llqq_c10_m3000.merge.AOD.e4175_e5984_s3126_r9364_r9315_tid12322351_00',
    #'mc16_13TeV.303297.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_ZZ_llqq_c10_m3000.merge.AOD.e4175_e5984_s3126_r10201_r10210_tid13573225_00',
    #'mc16_13TeV.303297.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_ZZ_llqq_c10_m3000.merge.AOD.e4175_e5984_s3126_r10724_r10726_tid15972381_00',
  ],


  'mc16_Jpsiee_boosted_nonresonant':[
    #B0 -> K*0 ee (300590): 
    'mc16_13TeV.300590.Py8BEG_A14_CTEQ6L1_Bd_Kstar_Kpi_e4e4.merge.AOD.e7074_e5984_s3126_r10724_r10726',
    #B0bar -> anti-K*0 ee (300591): 
    'mc16_13TeV.300591.Py8BEG_A14_CTEQ6L1_Bdbar_Kstar_Kpi_e4e4.merge.AOD.e7074_e5984_s3126_r10724_r10726_tid23143803_00',
  ],
  
  'mc16_Jpsiee_boosted_resonant' : [
    #B0 -> K*0 J/psi(ee)  (300592)
    'mc16_13TeV.300592.Py8BEG_A14_CTEQ6L1_Bd_Kstar_Kpi_Jpsi_e4e4.merge.AOD.e7074_e5984_s3126_r10724_r10726_tid16278975_00', 
    'mc16_13TeV.300592.Py8BEG_A14_CTEQ6L1_Bd_Kstar_Kpi_Jpsi_e4e4.merge.AOD.e7074_e5984_s3126_r10724_r10726_tid16279009_00',
    # B0bar -> anti-K*0- J/psi(ee) (300593) and 
    'mc16_13TeV.300593.Py8BEG_A14_CTEQ6L1_Bdbar_Kstar_Kpi_Jpsi_e4e4.merge.AOD.e7074_e5984_s3126_r10724_r10726_tid16279044_00',
    'mc16_13TeV.300593.Py8BEG_A14_CTEQ6L1_Bdbar_Kstar_Kpi_Jpsi_e4e4.merge.AOD.e7074_e5984_s3126_r10724_r10726_tid16279075_00',
  ],

  'mc15_Zee_20pt7_Correct2016TRTGasMixture' : [
    #'mcdir'    : 'mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.DAOD_EGAM1.e3601_s2876_r7886_r7676_p2613'
    #'mcdir'    : 'mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s2876_r7886_r7676'
    'mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s2876_r7917_r7676'
    ],

  'mc15_Jpsi_20pt7_Correct2016TRTGasMixture' : [
    #'mcdir'    : 'mc15_13TeV.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.merge.AOD.e3869_s2876_r7886_r7676'
    'mc15_13TeV.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.merge.AOD.e3869_s2876_r7917_r7676'
    ],


  'mc15_JF17_20pt7_Correct2016TRTGasMixture' : [
    #'jf17dir'  : 'mc15_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s2876_r7886_r7676_tid08236545_00'
    #'jf17dir'  : 'mc15_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s2876_r7886_r7676'
    'mc15_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s2876_r7917_r7676'
    ],


  'zee_mc16_with_ringer' : [
    'mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s3126_r9364_r9315',
    ],

  'jpsi_mc16_with_ringer' : [
    'mc16_13TeV.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.merge.AOD.e3869_s3126_r9364_r9315',
    ],


  'jf17_mc16_with_ringer' : [
    'mc16_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s3126_r9364_r9315',
    ],

  'zee_mc16d_with_ringer' : [
    'mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_e5984_a875_r10201_r10210',
    ],
    

  'jpsi_mc16d_with_ringer' : [
    'mc16_13TeV.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.merge.e3869_s3126_r10201_r10210',
    ],

  'jf17_mc16d_with_ringer' : [
    'mc16_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s3126_r10201_r10210',
    ],
  'jf35_mc16e_with_ringer': [
    'mc16_13TeV.423302.Pythia8EvtGen_A14NNPDF23LO_perf_JF35.merge.AOD.e3848_e5984_s3126_s3136_r10724_r10726'
   ],
  'jf50_mc16e_with_ringer': [
    'mc16_13TeV.423303.Pythia8EvtGen_A14NNPDF23LO_perf_JF50.merge.AOD.e3848_e5984_s3126_s3136_r10724_r10726'
   ],
  'DP8_17_mc16_with_ringer': [
    'mc16_13TeV.423099.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP8_17.merge.AOD.e4453_s3126_r9364_r9315'
  ],
  'DP17_35_mc16_with_ringer' : [
    'mc16_13TeV.423100.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP17_35.merge.AOD.e3791_s3126_r9364_r9315',
  ],
 'DP35_50_mc16_with_ringer': [
    'mc16_13TeV.423101.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP35_50.merge.AOD.e3904_s3126_r9364_r9315'
  ],
'DP50_70_mc16_with_ringer':[
    'mc16_13TeV.423102.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP50_70.merge.AOD.e3791_s3126_r9364_r9315'
],
'DP70_140_mc16_with_ringer':[
    'mc16_13TeV.423103.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP70_140.merge.AOD.e3791_s3126_r9364_r9315'
],
}
