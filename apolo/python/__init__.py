
__all__ = ['list2str', 'get_files', 'find_calib_path']

def list2str(l):
  s='['
  for ll in l:
    s+='\''+ll+'\','
  s+=']'
  return s


def get_files(dirtouse):
  import os
  finallist=[]
  while( dirtouse.endswith('/') ) :
       dirtouse= dirtouse.rstrip('/')
  listfiles=os.listdir(dirtouse)
  for ll in listfiles:
       finallist.append(dirtouse+'/'+ll)
  return finallist


def find_calib_path( path ):
  from PathResolver import PathResolver
  return PathResolver.FindCalibFile(path)


#from . import lxplus_data
#__all__.extend( lxplus_data.__all__ )
#from .lxplus_data import *

from . import Run2_mc
__all__.extend( Run2_mc.__all__ )
from .Run2_mc import *

from . import Run2_pp
__all__.extend(Run2_pp.__all__ )
from .Run2_pp import *

from . import Run2_triggers
__all__.extend(Run2_triggers.__all__ )
from .Run2_triggers import *


