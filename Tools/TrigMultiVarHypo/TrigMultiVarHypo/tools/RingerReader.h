/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
#ifndef TRIGMULTIVARHYPO_TOOLS_RINGERREADER_H
#define TRIGMULTIVARHYPO_TOOLS_RINGERREADER_H

#include "TrigMultiVarHypo/tools/Model.h"
#include "PathResolver/PathResolver.h"
#include "GaudiKernel/MsgStream.h"
#include <vector>
#include <string>
#include "TTree.h"
#include "TFile.h"

#include <TROOT.h>

  
///Helper class
class CutDefsHelper{
  private:
    double m_etamin;
    double m_etamax;
    double m_etmin;
    double m_etmax;
    double m_mumin;
    double m_mumax;
    // Define as [a, b, b0] where a and b came from y = ax+b and b0 is the cut without any correction
    std::vector<double> m_threshold;

  public:
    CutDefsHelper(std::vector<double> th, double etamin,float etamax,
                  double etmin, double etmax, double mumin, double mumax):m_etamin(etamin),
                  m_etamax(etamax),m_etmin(etmin),m_etmax(etmax),m_mumin(mumin),m_mumax(mumax),
                  m_threshold(th)
    {;}

    ~CutDefsHelper()
    {;}
    
    double threshold(){return m_threshold[2];};
    double threshold(double avgmu){return (avgmu*m_threshold[0] + m_threshold[1]);};
    double etamin(){return m_etamin;};
    double etamax(){return m_etamax;};
    double etmin(){ return m_etmin; };
    double etmax(){ return m_etmax; };
    double mumin(){ return m_mumin; };
    double mumax(){ return m_mumax; };
    double alpha(){return m_threshold[0];};
    double beta(){return m_threshold[1];};

    std::vector<double> getThresholdValues(){return m_threshold;};

    void setThresholdValues( std::vector<double> &th ){
      m_threshold.clear();
      for(unsigned i=0; i<th.size(); ++i)
        m_threshold.push_back(th.at(i));
    }




};///end of configuration
  

class RingerReader{

  public:
    /* constructor */
    RingerReader( std::string );
    /* destructor */
    ~RingerReader();
    /* use this methods to retrieve the tools from the archieve */
    bool retrieve(std::string &, std::vector<Model*> &);
    bool retrieve(std::string &, std::vector<CutDefsHelper*> &);
    /* return the current class name */
    std::string name(){return m_name;};
    
    int lumiCut(){return m_lumiCut;};
    
    
    bool  doPileupCorrection(){return m_doPileupCorrection;};
    bool  removeOutputTansigTF(){return m_removeOutputTansigTF;};
    bool  useTrack(){return m_useTrack;};
    bool  useCaloRings(){return m_useCaloRings;};
    bool  useShowerShape(){return m_useShowerShape;};
    bool  useEtaVar(){return m_useEtaVar;};
    bool  useLumiVar(){return m_useLumiVar;};
    
    void setMsgStream( MsgStream &msg ){m_log = new MsgStream(msg);};


  private:
    // Use this to retrieve all branch values
    template <class T>
    void InitBranch(TTree* fChain, std::string branch_name, T* param);
    
    MsgStream& msg() const{
       return (*m_log);
    }
    std::string m_name;
    int m_version;
    MsgStream *m_log;
    
    // archieve variables holder
    int  m_lumiCut;
    bool m_useEtaVar;
    bool m_useLumiVar;
    bool m_useCaloRings;
    bool m_useTrack;
    bool m_useShowerShape;
    bool m_removeOutputTansigTF;
    bool m_doPileupCorrection;

    // Dense Model
    std::vector<unsigned int>   *m_nodes;
    std::vector<double>         *m_weights;
    std::vector<double>         *m_bias;
    std::vector<double>         *m_thresholds;
    std::vector<std::string>    *m_tfnames;

    // Convolutional Model
    std::vector<bool>           *m_useConvLayer;
    std::vector<unsigned int>   *m_conv_nodes;
    std::vector<unsigned int>   *m_conv_kernel_i;
    std::vector<unsigned int>   *m_conv_kernel_j;
    std::vector<double>         *m_conv_kernel;
    std::vector<double>         *m_conv_bias;
    std::vector<unsigned int>   *m_conv_input_i;
    std::vector<unsigned int>   *m_conv_input_j;
    std::vector<std::string>    *m_conv_tfnames;
    std::vector<unsigned int>   *m_conv_frame;

    // Common parans
    std::vector<double>         *m_etaBins;
    std::vector<double>         *m_etBins;
    std::vector<double>         *m_muBins;
 

};//End of classi


#endif
