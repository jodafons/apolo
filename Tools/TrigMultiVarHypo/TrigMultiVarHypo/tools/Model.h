
#ifndef TRIGMULTIVARHYPO_TOOLS_MODEL_H
#define TRIGMULTIVARHYPO_TOOLS_MODEL_H

#include <vector>

typedef double REAL;


class Model{

  private:

    float m_etmin;
    float m_etmax;
    float m_etamin;
    float m_etamax;
    float m_mumin;
    float m_mumax;

    float m_output;
    float m_outputBeforeTheActivationFunction;
  
  public:
    
    
    // Constructor
    Model( REAL etmin, REAL etmax, REAL etamin, REAL etamax, REAL mumin, REAL mumax):
      m_etmin(etmin),m_etmax(etmax),m_etamin(etamin),m_etamax(etamax),m_mumin(mumin),m_mumax(mumax)
    {;};
    
    // Destructor
    virtual ~Model()
    {;};
  
    float getOutput(){return m_output;};

    float getOutputBeforeTheActivationFunction(){return m_outputBeforeTheActivationFunction;};

    void setOutput(float v){m_output=v;};

    void setOutputBeforeTheActivationFunction(float v){m_outputBeforeTheActivationFunction=v;};

    virtual float propagate( std::vector<float> & ){return 0;};
    
    REAL etamin(){ return m_etamin;};
    REAL etamax(){ return m_etamax;};
    REAL etmin(){  return m_etmin;};
    REAL etmax(){  return m_etmax;};
    REAL mumin(){  return m_mumin;};
    REAL mumax(){  return m_mumax;};
 
};

#endif
