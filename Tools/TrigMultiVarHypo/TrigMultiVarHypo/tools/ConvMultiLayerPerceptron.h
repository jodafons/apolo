
/*
 *
 *
 */

#ifndef TRIGMULTIVARHYPO_TOOLS_CONVMULTILAYERPERCEPTRON_H
#define TRIGMULTIVARHYPO_TOOLS_CONVMULTILAYERPERCEPTRON_H


#include <TROOT.h>
#include <string>
#include <vector>
#include "TrigMultiVarHypo/tools/Model.h"
#include "TrigMultiVarHypo/tools/MultiLayerPerceptron.h"


class Conv2D{

  private:

    REAL                  m_bias;
    std::vector<std::vector<REAL>>     m_kernel;
    unsigned int          m_kernel_i;
    unsigned int          m_kernel_j;
    unsigned int          m_input_i;
    unsigned int          m_input_j;
    std::string           m_tfname;
    
    
    unsigned int          m_output_i;
    unsigned int          m_output_j;
    REAL                **m_output;

    std::vector<REAL**>   m_inputs;

    void reset();
    /* Apply the selected TF in all positions from the output matrix*/
    void bias();
    void activation();

  public:

    // Constructor
    Conv2D( REAL bias, std::vector<std::vector<REAL>> &kernel, unsigned int ki, unsigned int kj, unsigned int ii, unsigned int ij, std::string &TF);    
    // Destructor
    ~Conv2D();
    // Propagate all inputs throut of the kernel
    void propagate( std::vector<REAL**> &);
    // Get the output pointer
    REAL** getOutput(){return m_output;};

    // force to release all allocated memory
    void release();
};




class ConvMultiLayerPerceptron: 
  public Model
{
  private:
    // dense fully connected layer
    MultiLayerPerceptron *m_dense;
    // Convolutional layers
    std::vector< std::vector< Conv2D * > > m_convLayers;
    // Reshape all inputs in a single vector
    std::vector<float> flatten( std::vector<REAL**> &inputs , unsigned int input_i, unsigned int input_j);

    void reshape2vortex( std::vector<float> &rings );

    void release();

    REAL      **m_rings;
    
    unsigned int   m_input_i;
    unsigned int   m_input_j;

    std::vector<unsigned int>  m_kernel_i;
    std::vector<unsigned int>  m_kernel_j;
    
    std::vector<unsigned int>  m_frame;

  public:
    
    // Constructor
    ConvMultiLayerPerceptron( 
         // dense layer
         std::vector<unsigned int>  &dense_n ,
         std::vector<REAL>          &dense_w,
         std::vector<REAL>          &dense_b,
         std::vector<std::string>   &dense_TF,
         // convolutional layer
         std::vector<unsigned int>  &conv_frame,
         unsigned int                conv_input_i,
         unsigned int                conv_input_j,
         std::vector<unsigned int>  &conv_n,
         std::vector<unsigned int>  &conv_kernel_i,
         std::vector<unsigned int>  &conv_kernel_j,
         std::vector<REAL>           conv_kernel, // (must be a copy)
         std::vector<REAL>           conv_bias, // (must be a copy)
         std::vector<std::string>   &conv_TF,
         // phase space parameters
         REAL etmin,
         REAL etmax,
         REAL etamin,
         REAL etamax,
         REAL mumin,
         REAL mumax
           );
    
    // Destructor
    ~ConvMultiLayerPerceptron();
  
    float propagate(std::vector<float> &input);
};



//!***************************************************************



#endif
