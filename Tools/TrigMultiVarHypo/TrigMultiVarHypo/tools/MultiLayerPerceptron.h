
/*
 * Neural Network Implementation v3.0
 * Developed by: Dhiana Deva Cavalcanti Rocha
 * Contact: dhiana.deva@gmail.com
 * Laboratorio de Processamento de Sinais
 * Universidade Federal do Rio de Janeiro
 *
 * Adapt by Joao victor da Fonseca Pinto:
 * jodafons@cern.ch
 */

#ifndef TRIGMULTIVARHYPO_TOOLS_MULTILAYERPERCEPTRON_H
#define TRIGMULTIVARHYPO_TOOLS_MULTILAYERPERCEPTRON_H

#define BAD_WEIGHT_SIZE   101
#define BAD_BIAS_SIZE     102

#include <TROOT.h>
#include <string>
#include <vector>
#include "TrigMultiVarHypo/tools/Model.h"


class MultiLayerPerceptron: public Model {

  private:

    std::vector<unsigned int> m_nodes; 
    REAL                   ***m_weights;
    REAL                    **m_bias;
    REAL                    **m_layerOutputs;
    REAL                    **m_neuronOutputs; // Use this to hold the neuron sum values before to apply the tansig function

    std::vector<std::string>  m_tfnames;

    REAL activation( REAL, std::string & );

  public:

    MultiLayerPerceptron(std::vector<unsigned int> &, 
                         std::vector<REAL> &, std::vector<REAL> &,
                         std::vector<std::string> &,
                         REAL etmin,  REAL etmax, 
                         REAL etamin,  REAL etamax,
                         REAL mumin,   REAL mumax);

    ~MultiLayerPerceptron();

    float propagate(std::vector<float> &input);

    
};


#endif
