/*
 *   Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
 *   */


#ifndef TrigEgammaL2CaloRingerSelectorTool_H
#define TrigEgammaL2CaloRingerSelectorTool_H

#include "TrigEgammaEmulationTool/ITrigEgammaSelectorBaseTool.h"
#include "TrigEgammaEmulationTool/TrigEgammaSelectorBaseTool.h"
#include "TrigMultiVarHypo/preproc/Preprocessor.h"
#include "TrigMultiVarHypo/tools/RingerHelper.h"
#include "TrigMultiVarHypo/tools/RingerReader.h"
#include "TrigMultiVarHypo/tools/MultiLayerPerceptron.h"
#include "TrigMultiVarHypo/tools/ConvMultiLayerPerceptron.h"
#include "TrigMultiVarHypo/tools/MultiLayerPerceptron.h"
#include "TrigMultiVarHypo/tools/Model.h"
#include "AsgTools/AsgTool.h"
#include <vector>

namespace Trig{
class TrigEgammaL2CaloRingerSelectorTool: 
  public Trig::TrigEgammaSelectorBaseTool,///TDT utilities
  virtual public Trig::ITrigEgammaSelectorBaseTool 
  {
  ASG_TOOL_CLASS(TrigEgammaL2CaloRingerSelectorTool,  Trig::ITrigEgammaSelectorBaseTool)

  public:
    using TrigEgammaSelectorBaseTool::emulation;
    //****************************************************************************** 
    TrigEgammaL2CaloRingerSelectorTool(const std::string& myname);
    ~TrigEgammaL2CaloRingerSelectorTool() {};

    StatusCode initialize();
    StatusCode finalize(); 
    bool emulation( const xAOD::TrigEMCluster*, bool &, const Trig::Info &);

  private:
    ///Thresholds Holder
    std::vector<CutDefsHelper*>  m_cutDefs; 
    ///Discriminator holder
    std::vector<Model*>   m_discriminators;
    ///Pre-processing holder
    std::vector<Preprocessor*> m_preproc; 

    RingerReader m_reader;
    
    /* Helper method to retrieve the bins from an index */
    //void index_to_et_eta_bins(unsigned, unsigned &, unsigned &);
    void setEtThr( float et ){m_etCut=et;};
    
    float       m_etCut;
    float       m_output;
    float       m_lumiCut;
    bool        m_doPileupCorrection;
    bool        m_removeOutputTansigTF;
    bool        m_useLumiTool;

    //Prepoc configuration
    std::vector<unsigned int>            m_nRings;
    std::vector<unsigned int>            m_normRings;
    std::vector<unsigned int>            m_sectionRings;

    //Discriminator configuration
    std::string m_calibPath_constants, m_calibPath_thresholds;

  };

}//namespace

#endif
