#!/usr/bin/env python

import argparse

from Gaugi.messenger import Logger, LoggingLevel

mainLogger = Logger.getModuleLogger("RunFRMonitoringParser")

parser = argparse.ArgumentParser(description = '', add_help = False)
parser = argparse.ArgumentParser()

parser.add_argument('-n','--nov', action='store', 
    dest='nov', required = False, default = 100, type=int,
    help = "Number of events.")

parser.add_argument('--grid', action='store_true', 
    dest='grid', required = False, default = False,
    help = "to launch on the grid.")

parser.add_argument('-c','--GridName', action='store', 
    dest='GridName', required = False, type=str,
    help = "The name of the dict in GridFiles.")

parser.add_argument('--pidVersion', action='store', 
    dest='pidVersion', required = False, type=str, default=None,
    help = "Set pid version manually.")

parser.add_argument('--ringerVersion', action='store', 
    dest='ringerVersion', required = False, type=str, default=None,
    help = "Set ringer version manually.")

parser.add_argument('--pp_2017', action='store_true', 
    dest='pp_2017', required = False, default = False,
    help = "Use the likelihood 2017 P1 tuning in the emulation step.")

parser.add_argument('--pp_2018', action='store_true', 
    dest='pp_2018', required = False, default = False,
    help = "Use the likelihood 2018 P1 tuning in the emulation step.")

parser.add_argument('--v9', action='store_true', 
    dest='v9', required = False, default = False,
    help = "Use the ringer v9 tuning in the emulation step.")

parser.add_argument('--v8', action='store_true', 
    dest='v8', required = False, default = False,
    help = "Use the ringer v8 tuning in the emulation step.")

parser.add_argument('--v6', action='store_true', 
    dest='v6', required = False, default = False,
    help = "Use the ringer v6 tuning in the emulation step")

#parser.add_argument('--test_eb_data16', action='store_true', 
#    dest='eb_data16', required = False, default = False,
#    help = "Test the EnhancedBias 2016 sample.")

parser.add_argument('--test_data18', action='store_true', 
    dest='data18', required = False, default = False,
    help = "Test the EGAM7 2018 sample.")

parser.add_argument('--test_data17', action='store_true', 
    dest='data17', required = False, default = False,
    help = "Test the EGAM7 2017 sample.")


parser.add_argument('--dry_run', action='store_true', 
    dest='dry_run', required = False, default = False,
    help = "Do not launch if this is True.")

parser.add_argument('-e','--do_emulation', action='store_true', 
    dest='do_emulation', required = False, default = False,
    help = "Attach the emulation tool into the athena stack.")

parser.add_argument('-t','--tag', action='store', 
    dest='tag', required = False, type=str, default=None,
    help = "Tag to be append in the and of the output container (just for grid)")

parser.add_argument('--runFlag', action='store', 
    dest='runFlag', required = False, type=str, default='2018',
    help = "Set the offline calibpaths from analysis using the runFlag (2017 OR 2018)")

parser.add_argument('--nEventsPerJob', action='store', 
    dest='nEventsPerJob', required = False, type=int, default=4000,
    help = "Number of evetns per job in grid mode.")


parser.add_argument('--Ringer2017CommissioningPeriod', action='store_true', 
    dest='Ringer2017CommissioningPeriod', required = False, default = False,
    help = "Ringer commissioning period (period A to B in 2017)")

parser.add_argument('--doFakes', action='store_true', 
    dest='doFakes', required = False, default = False,
    help = "Use veto veryloose in the event selection (only for fakes measurements).")

from TrigEgammaJobConfigs.utilities import list2str
import sys,os
if len(sys.argv)==1:
  parser.print_help()
  sys.exit(1)


args = parser.parse_args()


_args='"RunFlag=\'{RUN_FLAG}\''.format(RUN_FLAG=args.runFlag)

if not args.ringerVersion:
  if args.v9:
    # develpment tuning configuration (not official)
    args.ringerVersion='TrigEgammaEmulationTool/RingerSelectorTools/trigger/data17_20180823_v9'
    _args+=';ringerVersion=\'{RINGER_VERSION}\''.format(RINGER_VERSION=args.ringerVersion)
    mainLogger.warning('This is a development tuning and this is not OFFICIAL.')
  elif args.v8:
    args.ringerVersion='RingerSelectorTools/TrigL2_20180125_v8/'
    _args+=';ringerVersion=\'{RINGER_VERSION}\''.format(RINGER_VERSION=args.ringerVersion)
  elif args.v6:
    args.ringerVersion='RingerSelectorTools/TrigL2_20170505_v6'
    _args+=';ringerVersion=\'{RINGER_VERSION}\''.format(RINGER_VERSION=args.ringerVersion)
  else:
    mainLogger.info('ringerVersion not setted. Using the default property from runFlag configuration')


if not args.pidVersion:
  # Use this as default
  if args.pp_2017:
    args.pidVersion='ElectronPhotonSelectorTools/trigger/rel21_20170217/'
    _args+=';pidVersion=\'{PID_VERSION}\''.format(PID_VERSION=args.pidVersion)
  elif args.pp_2018:
    args.pidVersion='ElectronPhotonSelectorTools/trigger/rel21_20180312/'
    _args+=';pidVersion=\'{PID_VERSION}\''.format(PID_VERSION=args.pidVersion)
  else:
    mainLogger.info('pidVersion not setted. Using the default property from runFlag configuration')



if args.ringerVersion:
  mainLogger.info('Using ringerVersion as %s',args.ringerVersion)
if args.pidVersion:
  mainlogger.info('Using pidVersion as %s',args.pidVersion)


grls=[]
if args.runFlag=='2017':
  grls.append( 'TrigEgammaJobConfigs/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml' )
elif args.runFlag=='2018':
  grls.append( 'TrigEgammaJobConfigs/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml' )
else:
  mainLogger.warning('runFlag wrong configuration')




grl_str=str()
for grl in grls:
  #if not os.path.exists(grl):
  #  os.system('cp JobConfig/data/%s .'%grl)
  mainLogger.info('Adding GRL to the list: %s',grl)
  grl_str+='\''+grl+'\','
_args+=';GRL=['+grl_str+']'


# setting emulatio flag
if args.do_emulation:
  _args+=';do_emulation=True'


if args.v9:
  _args+=';DoRingerBelow15GeV=True'


if args.doFakes:
  _args+=';DoVetoVeryLoose=True'


if args.runFlag=='2017':
  if args.Ringer2017CommissioningPeriod:
    mainLogger.info('Ringer commissioning period!')

    from TrigEgammaJobConfigs import monitoring_2017_ringer_commissioning
    _args+=';monitoring_list = '+list2str(monitoring_2017_ringer_commissioning)
  else:
    from TrigEgammaJobConfigs import monitoring_2017
    _args+=';monitoring_list = '+list2str(monitoring_2017)

elif args.runFlag=='2018':
  from TrigEgammaJobConfigs import monitoring_2018
  _args+=';monitoring_list = '+list2str(monitoring_2018)
else:
  mainLogger.error('Wrong runFlag configuration.')




if args.grid:

  _args+='"'
  command = """pathena -c {ARGS} TrigEgammaJobConfigs/RunMonitoring.py \\
              --inDS={INDS} \\
              --outDS={OUTDS} \\
              --nEventsPerJob={NEVENTSPERJOB} \\
              --supStream=EXPERT,SHIFT,run_1,RUNSTAT,DEBUG \\
              --mergeOutput \\
              --extOutFile '*.xml' \\
              """

  from TrigEgammaJobConfigs import grid_mc_files, grid_pp_files
  if 'mc' in args.GridName:
    files = grid_mc_files[args.GridName] 
  else:
    files = grid_pp_files[args.GridName] 

  for idx, inds in enumerate(files):
    mainLogger.info('container %d/%d',idx+1,len(files))
    if ':' in inds: inds=inds.split(':')[1]
    outds = 'user.{USER}.{INDS}.{TAG}'.format(USER=os.environ['USER'],
                                              INDS=inds,
                                              TAG=args.tag)
    print command.format(ARGS=_args, INDS=inds, OUTDS=outds, NEVENTSPERJOB=args.nEventsPerJob)
    
    if not args.dry_run:
      os.system(command.format(ARGS=_args, INDS=inds, OUTDS=outds, NEVENTSPERJOB=args.nEventsPerJob))

else:
  # extra arguments to the job
  _args += ';NOV=%d'%args.nov
  if args.data18:
    _args+=";test_job=\'data18\'"
  elif args.data17:
    _args+=";test_job=\'data17\'"
  else:
    mainLogger.warning('option not reconized')
  _args+='"'
  command = 'athena -c {ARGS} TrigEgammaJobConfigs/RunMonitoring.py &> mylog.log'.format(ARGS=_args)
  print command

  if not args.dry_run:
    os.system(command)





