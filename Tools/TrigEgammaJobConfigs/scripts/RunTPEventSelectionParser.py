#!/usr/bin/env python

import argparse
from Gaugi.messenger import LoggingLevel, Logger
mainLogger = Logger.getModuleLogger("RunTPEventSelectionParser")


parser = argparse.ArgumentParser(description = '', add_help = False)
parser = argparse.ArgumentParser()

parser.add_argument('-n','--nov', action='store',
    dest='nov', required = False, default = 100, type=int,
    help = "Number of events.")

parser.add_argument('--grid', action='store_true',
    dest='grid', required = False, default = False,
    help = "to launch on the grid.")

parser.add_argument('--ganga', action='store_true',
    dest='ganga', required = False, default = False,
    help = "to use ganga.")

parser.add_argument('-c','--gridName', action='store',
    dest='GridName', required = False, type=str, default=None,
    help = "The name of the key in GridFiles.")

parser.add_argument('--pidVersion', action='store',
    dest='pidVersion', required = False, type=str, default=None,
    help = "Set pid version manually.")

parser.add_argument('--ringerVersion', action='store',
    dest='ringerVersion', required = False, type=str, default=None,
    help = "Set ringer version manually.")

parser.add_argument('--runFlag', action='store',
    dest='runFlag', required = False, type=str, default='2018',
    help = "Set the offline calibpaths from analysis using the runFlag (2017 OR 2018)")

parser.add_argument('--pp_2017', action='store_true',
    dest='pp_2017', required = False, default = False,
    help = "Use the likelihood 2017 P1 tuning in the emulation step.")

parser.add_argument('--pp_2018', action='store_true',
    dest='pp_2018', required = False, default = False,
    help = "Use the likelihood 2018 P1 tuning in the emulation step.")

parser.add_argument('--v9', action='store_true',
    dest='v9', required = False, default = False,
    help = "Use the ringer v9 tuning in the emulation step.")

parser.add_argument('--v8', action='store_true',
    dest='v8', required = False, default = False,
    help = "Use the ringer v8 tuning in the emulation step.")

parser.add_argument('--v6', action='store_true',
    dest='v6', required = False, default = False,
    help = "Use the ringer v6 tuning in the emulation step")

parser.add_argument('--test_egam1', action='store_true',
    dest='egam1', required = False, default = False,
    help = "Test and egamma 1 sample.")

parser.add_argument('--test_egam2', action='store_true',
    dest='egam2', required = False, default = False,
    help = "Test an egamma 2 sample")

parser.add_argument('--test_egam7', action='store_true',
    dest='egam7', required = False, default = False,
    help = "Test an egamma 7 sample.")

parser.add_argument('--test_data18', action='store_true',
    dest='data18', required = False, default = False,
    help = "Test an data18 AOD for Zee and Jpsiee.")

parser.add_argument('--test_EB', action='store_true',
    dest='EB', required = False, default = False,
    help = "Test an EB AOD for fake.")

parser.add_argument('--dry_run', action='store_true',
    dest='dry_run', required = False, default = False,
    help = "Do not launch if this is True.")

parser.add_argument('-e','--do_emulation', action='store_true',
    dest='do_emulation', required = False, default = False,
    help = "Attach the emulation tool into the athena stack.")

parser.add_argument('-t','--tag', action='store',
    dest='tag', required = False, type=str, default=None,
    help = "Tag to be append in the and of the output container (just for grid)")

parser.add_argument('--nEventsPerJob', action='store',
    dest='nEventsPerJob', required = False, type=int, default=4000,
    #dest='nEventsPerJob', required = False, type=int, default=10000,
    help = "Number of evetns per job in grid mode.")

#parser.add_argument('--maxEvents', action='store',
#    dest='maxEvents', required = False, type=int, default=-1,
#    help = "Number of max evetns in grid mode.")



parser.add_argument('--zee', action='store_true',
    dest='zee', required = False, default = False,
    help = "Attach the emulation tool into the athena stack.")
parser.add_argument('--fake', action='store_true',
    dest='fake', required = False, default = False,
    help = "Attach the emulation tool into the athena stack.")
parser.add_argument('--jpsiee',  action='store_true',
    dest='jpsiee', required = False, default = False,
    help = "Attach the emulation tool into the athena stack.")

from TrigEgammaJobConfigs import list2str


import sys,os
if len(sys.argv)==1:
  parser.print_help()
  sys.exit(1)


args = parser.parse_args()


_args='"RunFlag=\'{RUN_FLAG}\''.format(RUN_FLAG=args.runFlag)

if not args.ringerVersion:
  if args.v9:
    # develpment tuning configuration (not official)
    #args.ringerVersion='TrigEgammaEmulationTool/RingerSelectorTools/trigger/data17_20180823_v9'
    args.ringerVersion='tunings/RingerSelectorTools/trigger/data17_20190309_v9'
    _args+=';ringerVersion=\'{RINGER_VERSION}\''.format(RINGER_VERSION=args.ringerVersion)
    mainLogger.warning('This is a development tuning and this is not OFFICIAL.')
  elif args.v8:
    args.ringerVersion='RingerSelectorTools/TrigL2_20180125_v8/'
    _args+=';ringerVersion=\'{RINGER_VERSION}\''.format(RINGER_VERSION=args.ringerVersion)
  elif args.v6:
    args.ringerVersion='RingerSelectorTools/TrigL2_20170505_v6'
    _args+=';ringerVersion=\'{RINGER_VERSION}\''.format(RINGER_VERSION=args.ringerVersion)
  else:
    mainLogger.info('ringerVersion not setted. Using the default property from runFlag configuration')


if not args.pidVersion:
  # Use this as default
  if args.pp_2017:
    args.pidVersion='ElectronPhotonSelectorTools/trigger/rel21_20170217/'
    _args+=';pidVersion=\'{PID_VERSION}\''.format(PID_VERSION=args.pidVersion)
  elif args.pp_2018:
    args.pidVersion='ElectronPhotonSelectorTools/trigger/rel21_20180312/'
    _args+=';pidVersion=\'{PID_VERSION}\''.format(PID_VERSION=args.pidVersion)
  else:
    mainLogger.info('pidVersion not setted. Using the default property from runFlag configuration')



if args.ringerVersion:
  mainLogger.info('Using ringerVersion as %s',args.ringerVersion)
if args.pidVersion:
  mainlogger.info('Using pidVersion as %s',args.pidVersion)


grls=[]
if args.runFlag=='2017':
  grls.append( 'TrigEgammaJobConfigs/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml' )
elif args.runFlag=='2018':
  grls.append( 'TrigEgammaJobConfigs/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml' )
else:
  mainlogger.warning( 'runFlag wrong configuration')




# check if GRL is in the current path
grl_str=str()
for grl in grls:
  #if not os.path.exists(grl):
  #  if args.ganga:
  #    os.system('cp ../data/%s .'%grl)
  #  else:
  #    os.system('cp JobConfig/data/%s .'%grl)
  mainLogger.info('Adding GRL to the list: %s',grl)
  grl_str+='\''+grl+'\','
_args+=';GRL=['+grl_str+']'


# setting emulatio flag
if args.do_emulation:
  _args+=';do_emulation=True'


if args.v9:
  _args+=';DoRingerBelow15GeV=True'




from TrigEgammaJobConfigs import monitoringTP_Zee_2018, monitoringTP_Zee_2018_tags, \
    monitoringTP_Jpsiee_2018_tags, monitoringTP_Jpsiee_2018

if args.zee:
  selection_method = 'Zee'
  _args+=';monitoringTP = '+list2str(monitoringTP_Zee_2018)
  _args+=';monitoringTP_tags = '+list2str(monitoringTP_Zee_2018_tags)
elif args.fake:
  selection_method = 'Fake'
  _args+=';monitoringTP = '+list2str(monitoringTP_Zee_2018+monitoringTP_Jpsiee_2018)
  _args+=';monitoringTP_tags = '+list2str(monitoringTP_Zee_2018_tags)
elif args.jpsiee:
  selection_method = 'Jpsiee'
  _args+=';monitoringTP = '+list2str(monitoringTP_Jpsiee_2018)
  _args+=';monitoringTP_tags = '+list2str(monitoringTP_Jpsiee_2018_tags)
else:
  selection_method = 'Zee'
  _args+=';monitoringTP = '+list2str(monitoringTP_Zee_2018)
  _args+=';monitoringTP_tags = '+list2str(monitoringTP_Zee_2018_tags)

_args+=';Selection=\'{SELECTION_METHOD}\''.format(SELECTION_METHOD=selection_method)



if args.grid:

  _args+='"'
  command = """pathena -c {ARGS} TrigEgammaJobConfigs/RunTPEventSelection.py \\
              --inDS={INDS} \\
              --outDS={OUTDS} \\
              --nEventsPerJob={NEVENTSPERJOB} \\
              --supStream=EXPERT,SHIFT,run_1,RUNSTAT,DEBUG \\
              --mergeOutput \\
              --extOutFile '*.xml' \\
              """

              #--mergeOutput \\
  if args.egam2:
    files = ['data17_13TeV.00330025.physics_Main.deriv.DAOD_EGAM2.f843_m1824_p3336']
  elif args.egam1:
    files = ['data17_13TeV.00327342.physics_Main.deriv.DAOD_EGAM1.f838_m1824_p3336']
  elif args.egam7:
    files = ['data17_13TeV.00338498.physics_Main.deriv.DAOD_EGAM7.f877_m1892_p3336']
  elif args.data18:
    files = ['data18_13TeV.00349592.physics_Main.merge.AOD.f937_m1972']
  else:
    from TrigEgammaJobConfigs import grid_mc_files, grid_pp_files
    if 'mc' in args.GridName:
      files = grid_mc_files[args.GridName]
    else:
      files = grid_pp_files[args.GridName]

  for idx, inds in enumerate(files):

    mainLogger.info( 'container %d/%d', idx+1,len(files))
    if ':' in inds: inds=inds.split(':')[1]

    outds = 'user.{USER}.{INDS}.{TAG}'.format(USER=os.environ['USER'],
                                              INDS=inds,
                                              TAG=args.tag)
    print command.format(ARGS=_args, INDS=inds, OUTDS=outds, NEVENTSPERJOB=args.nEventsPerJob)

    if not args.dry_run:
      os.system(command.format(ARGS=_args, INDS=inds, OUTDS=outds, NEVENTSPERJOB=args.nEventsPerJob))


elif args.ganga:
  #_args += ';FILE=%IN'
  _args+='"'
  command = """/afs/cern.ch/user/m/mslater/public/Ganga/install/6.7.4/bin/ganga athena --lsf --queue 8nh --nocompile  --inputtype DQ2\\
      --splitfiles=1 \\
      --outputdata "monitoring.root" \\
      --outputlocation="/afs/cern.ch/work/j/jodafons/WorkEmulation/ganga" \\
      --athena_exe 'EXE' "athena -c \"{ARGS}\" TrigEgammaJobConfigs/RunTPEventSelection.py" \\
      --inDS={INDS} \\
      """
  if args.egam2:
    #files = ['data17_13TeV.00330025.physics_Main.deriv.DAOD_EGAM2.f843_m1824_p3336']
    files = ['data17_13TeV.00330025.physics_Main.deriv.DAOD_EGAM2.f843_m1824_p3336_tid12443604_00']
  elif args.egam1:
    files = ['data17_13TeV.00327342.physics_Main.deriv.DAOD_EGAM1.f838_m1824_p3336']
  elif args.egam7:
    files = ['data17_13TeV.00338498.physics_Main.deriv.DAOD_EGAM7.f877_m1892_p3336']
  elif args.data18:
    files = ['data18_13TeV.00349592.physics_Main.merge.AOD.f937_m1972']
  else:
    from TrigEgammaJobConfigs  import grid_mc_files, grid_pp_files
    if 'mc' in args.GridName:
      files = grid_mc_files[args.GridName]
    else:
      files = grid_pp_files[args.GridName]

  for idx, inds in enumerate(files):

    if ':' in inds: inds=inds.split(':')[1]

    outds = 'user.{USER}.{INDS}.{TAG}'.format(USER=os.environ['USER'],
                                              INDS=inds,
                                              TAG=args.tag)
    print command.format(ARGS=_args, INDS=inds)#, OUTDS=outds)

    if not args.dry_run:
      os.system(command.format(ARGS=_args, INDS=inds))#, OUTDS=outds))

    if args.njobs and idx > args.njobs:
      break


#'athena -c {ARGS} RunCostMonitoring.py &> mylog.log'.format(ARGS=_args)
 # print command.format(ARGS=_args)
 # if not args.dry_run:
 #   os.system(command.format(ARGS=_args))

else:
  #extra arguments to the job
  _args += ';NOV=%d'%args.nov
  if args.egam1:
    _args+=";test_job=\'EGAM1\'"
  elif args.egam2:
    _args+=";test_job=\'EGAM2\'"
  elif args.egam7:
    _args+=";test_job=\'EGAM7\'"
  elif args.data18:
    _args+=";test_job=\'data18\'"
  elif args.EB:
    _args+=";test_job=\'EB\'"
  else:
    print 'option not reconized'
  _args+='"'

  print _args
  command = 'athena -c {ARGS} TrigEgammaJobConfigs/RunTPEventSelection.py &> mylog.log'.format(ARGS=_args)
  print command

  if not args.dry_run:
    os.system(command)

















