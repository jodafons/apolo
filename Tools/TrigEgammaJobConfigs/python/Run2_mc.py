
__all__  = ['grid_mc_files']

grid_mc_files = {

  'mc15_Zee_20pt7_Correct2016TRTGasMixture' : [
    #'mcdir'    : 'mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.DAOD_EGAM1.e3601_s2876_r7886_r7676_p2613'
    #'mcdir'    : 'mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s2876_r7886_r7676'
    'mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s2876_r7917_r7676'
    ],

  'mc15_Jpsi_20pt7_Correct2016TRTGasMixture' : [
    #'mcdir'    : 'mc15_13TeV.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.merge.AOD.e3869_s2876_r7886_r7676'
    'mc15_13TeV.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.merge.AOD.e3869_s2876_r7917_r7676'
    ],


  'mc15_JF17_20pt7_Correct2016TRTGasMixture' : [
    #'jf17dir'  : 'mc15_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s2876_r7886_r7676_tid08236545_00'
    #'jf17dir'  : 'mc15_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s2876_r7886_r7676'
    'mc15_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s2876_r7917_r7676'
    ],


  'zee_mc16_with_ringer' : [
    'mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s3126_r9364_r9315',
    ],

  'jpsi_mc16_with_ringer' : [
    'mc16_13TeV.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.merge.AOD.e3869_s3126_r9364_r9315',
    ],


  'jf17_mc16_with_ringer' : [
    'mc16_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s3126_r9364_r9315',
    ],

  'zee_mc16d_with_ringer' : [
    'mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_e5984_a875_r10201_r10210',
    ],
    

  'jpsi_mc16d_with_ringer' : [
    'mc16_13TeV.423200.Pythia8B_A14_CTEQ6L1_Jpsie3e3.merge.e3869_s3126_r10201_r10210',
    ],

  'jf17_mc16d_with_ringer' : [
    'mc16_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.merge.AOD.e3848_s3126_r10201_r10210',
    ],
  }



