
__all__ = ['monitoringTP_Zee_2017_tags',
           'monitoringTP_Zee_2018_tags',
           'monitoringTP_Jpsiee_2018_tags',
           'monitoringTP_Jpsiee_2018_tags',
           'monitoringTP_Zee_2017_ringer_commissioning',
           'monitoringTP_Zee_2017',
           'monitoringTP_Zee_2018',
           'monitoringTP_Jpsiee_2018',
           'monitoringTP_Jpsiee_2017',
           # fake analysis
           'monitoring_2017_ringer_commissioning',
           'monitoring_2017',
           'monitoring_2018',
           ]

monitoringTP_Zee_2017_tags = ['HLT_e24_lhtight_nod0_ivarloose',
                              'HLT_e26_lhtight_nod0_ivarloose',
                              'HLT_e28_lhtight_nod0_ivarloose']

monitoringTP_Zee_2018_tags = monitoringTP_Zee_2017_tags



monitoringTP_Zee_2017 = [
                   'HLT_e17_lhvloose_nod0_L1EM15VHI',
                   'HLT_e17_lhloose_nod0',
                   #'HLT_e17_lhloose_nod0_noringer',
                   'HLT_e24_lhvloose_nod0_L1EM20VH',
                   #'HLT_e24_lhtight_nod0_L1EM20VH',
                   'HLT_e26_lhmedium_nod0',
                   #'HLT_e26_lhtight_nod0',
                   'HLT_e26_lhtight_nod0_ivarloose',
                   #'HLT_e26_lhtight_nod0_noringer_ivarloose',
                   #'HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM',
                   'HLT_e28_lhtight_nod0',
                   'HLT_e28_lhtight_nod0_ivarloose',
                   'HLT_e28_lhtight_nod0_noringer_ivarloose',
                   'HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM',
                   'HLT_e60_lhmedium_nod0',
                   #'HLT_e60_lhmedium_nod0_noringer',
                   'HLT_e60_lhmedium_nod0_L1EM24VHI',
                   'HLT_e140_lhloose_nod0',
                   #'HLT_e140_lhloose_nod0_noringer',
                   'HLT_e140_lhloose_nod0_L1EM24VHI',
                    ]



monitoringTP_Zee_2017_ringer_commissioning = [
          'HLT_e17_lhvloose_nod0_L1EM15VHI',# (in rerun mode for HLT_2e17_lhvloose_nod0_L12EM15VHI)
          'HLT_e17_lhvloose_nod0_ringer_L1EM15VHI',# (in rerun mode for HLT_2e17_lhvloose_nod0_L12EM15VHI)
          'HLT_e17_lhloose_nod0',# (in rerun mode for HLT_e17_lhloose_nod0_mu14)
          'HLT_e17_lhloose_nod0_ringer',# (in rerun mode for HLT_e17_lhloose_nod0_mu14)
          'HLT_e24_lhvloose_nod0_L1EM20VH',# (in rerun mode for HLT_2e24_lhvloose_nod0)
          'HLT_e24_lhvloose_nod0_ringer_L1EM20VH',# (in rerun mode for HLT_2e24_lhvloose_nod0)
          'HLT_e26_lhmedium_nod0',# (in rerun mode for HLT_e26_lhmedium_nod0_mu8noL1)
          'HLT_e26_lhmedium_nod0_ringer',# (in rerun mode for HLT_e26_lhmedium_nod0_mu8noL1)
          'HLT_e26_lhtight_nod0_ivarloose',
          'HLT_e26_lhtight_nod0_ringer_ivarloose',
          'HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM',
          'HLT_e26_lhtight_nod0_ringer_ivarloose_L1EM22VHIM',
          'HLT_e28_lhtight_nod0_ivarloose',
          'HLT_e28_lhtight_nod0_ringer_ivarloose',
          'HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM',
          'HLT_e28_lhtight_nod0_ringer_ivarloose_L1EM24VHIM',
          'HLT_e60_lhmedium_nod0',
          'HLT_e60_lhmedium_nod0_ringer',
          'HLT_e60_lhmedium_nod0_L1EM24VHI',
          'HLT_e60_lhmedium_nod0_ringer_L1EM24VHI',
          'HLT_e140_lhloose_nod0',
          'HLT_e140_lhloose_nod0_ringer',
          'HLT_e140_lhloose_nod0_L1EM24VHI',
          'HLT_e140_lhloose_nod0_ringer_L1EM24VHI',
              ]






monitoring_2017 = monitoringTP_Zee_2017
monitoring_2017_ringer_commissioning = monitoringTP_Zee_2017_ringer_commissioning

monitoringTP_Zee_2018 = monitoringTP_Zee_2017
monitoring_2018 = monitoringTP_Zee_2017


monitoringTP_Jpsiee_2018_tags = [
    'HLT_e5_lhtight_nod0_e4_etcut_Jpsiee_L1JPSI-1M5',
    'HLT_e9_etcut_e5_lhtight_nod0_Jpsiee_L1JPSI-1M5-EM7',
    'HLT_e14_etcut_e5_lhtight_nod0_Jpsiee_L1JPSI-1M5-EM12',
    'HLT_e9_lhtight_nod0_e4_etcut_Jpsiee_L1JPSI-1M5-EM7',
    'HLT_e14_lhtight_nod0_e4_etcut_Jpsiee_L1JPSI-1M5-EM12',
]
monitoringTP_Jpsiee_2017_tags = monitoringTP_Jpsiee_2018_tags

monitoringTP_Jpsiee_2018 =  [
  'HLT_e5_lhloose_nod0',
  'HLT_e10_lhvloose_nod0_L1EM7',
  'HLT_e5_lhvloose_nod0',
  'HLT_e12_lhvloose_nod0_L1EM10VH',
  'HLT_e7_lhmedium_nod0',
  'HLT_e9_lhloose_nod0',
  'HLT_e9_lhmedium_nod0',
  'HLT_e12_lhmedium_nod0',
  'HLT_e5_lhtight_nod0',
  'HLT_e9_lhtight_nod0',
  'HLT_e14_lhtight_nod0',
]

monitoringTP_Jpsiee_2017 = monitoringTP_Jpsiee_2018

                            #"HLT_e5_lhtight_nod0",
                            #"HLT_e5_lhtight_nod0_ringer",
                            #"HLT_e12_lhloose_nod0",
                            #"HLT_e12_lhloose_nod0_ringer"
 #                           ]





