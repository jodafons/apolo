
__all__ = ['lxplus_data_test_path_resolver']




def lxplus_data_test_path_resolver(): 
  import os
  username = os.environ['USER']
  basepath = '/afs/cern.ch/work/%s/%s/public/data_samples'% (username[0], username)
  from TrigEgammaJobConfigs.utilities import get_files 
  lxplus_data = {
    'EGAM1' : get_files( basepath+'/data17_13TeV.00338349.physics_Main.deriv.DAOD_EGAM1.f877_m1885_p3336'),
    'EGAM2' : get_files( basepath+'/data17_13TeV.00339070.physics_Main.deriv.DAOD_EGAM2.f887_m1897_p3336'),
    'EGAM7' : get_files( basepath+'/data17_13TeV.00335131.physics_Main.deriv.DAOD_EGAM7.f868_m1870_p3336'),
    'data18': get_files( basepath+'/data18_13TeV.00349592.physics_Main.merge.AOD.f937_m1972'),
    'EB'    : get_files( basepath+'/data17_13TeV.00360026.physics_EnhancedBias.merge.AOD.r10556_r10569_r10627_p3516_tid15512218_00'),
    }
  return lxplus_data

