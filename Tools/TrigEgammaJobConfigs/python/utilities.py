

__all__ = ['list2str', 'get_files', 'FindCalibPath']

def list2str(l):
  s='['
  for ll in l:
    s+='\''+ll+'\','
  s+=']'
  return s


def get_files(dirtouse):
  import os
  finallist=[]
  while( dirtouse.endswith('/') ) :
       dirtouse= dirtouse.rstrip('/')
  listfiles=os.listdir(dirtouse)
  for ll in listfiles:
       finallist.append(dirtouse+'/'+ll)
  return finallist


def FindCalibPath( path ):
  from PathResolver import PathResolver
  return PathResolver.FindCalibFile(path)
