
####################################################################################################
# Job options for standalone and Tier0 running of AnalysisTools
# Authors:
# Ryan Mackenzie White <ryan.white@cern.ch>
# Joao Victor Pinto    <jodafons@cern.ch>
#
# Tool and algorithm configuration done using egamma Factories
#
# To run
# athena -l DEBUG -c "DIR='/afs/cern.ch/work/j/jolopezl/datasets/\
#                     valid1.147406.PowhegPythia8_AZNLO_Zee.recon.AOD.e3099_s2578_r6220_tid05203475_00'"
#                 -c "NOV=50" test_ZeeElectronLowPtSupportingTrigAnalysis.py
# where NOV is the number of events to run
####################################################################################################

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
from RecExConfig.RecFlags import rec
from RecExConfig.RecAlgsFlags import recAlgs
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags as acf
from glob import glob
import os



finallist = []
grllist = []

if 'monitoringTP_tags' in dir():
  _monitoringTP_tags = monitoringTP_tags
else:
  print 'WARNING: There is no trigger for tags list.'
  _monitoringTP_tags = []

if 'monitoringTP' in dir():
  _monitoringTP = monitoringTP
else:
  print 'WARNING: There is no trigger for probe list.'
  _monitoringTP = []







if 'FILE' in dir() :
  finallist.append(FILE)

if 'DIR' in dir() :
  from TrigEgammaJobConfigs import get_files
  finallist.extend( get_files(DIR) )

if 'NOV' in dir():
  nov=NOV
else :
  nov=100

if 'GRL' in dir():
  doGRL=True
  if type(GRL) is str:
    grllist.append(GRL)
  else:
    grllist.extend(GRL)
else:
  doGRL=False

if 'do_emulation' in dir():
  doEmulation=do_emulation
else:
  doEmulation=False


if 'DoRingerBelow15GeV' in dir():
  doRingerBelow15GeV=DoRingerBelow15GeV
else:
  doRingerBelow15GeV=False


if 'RunFlag' in dir():
  runFlag=RunFlag
else:
  runFlag='2018'


if 'test_job' in dir():
  from TrigEgammaJobConfigs import lxplus_data_test_path_resolver
  lxplus_data = lxplus_data_test_path_resolver()
  try:
    finallist.extend( lxplus_data[test_job] )
  except e:
    raise RuntimeError('Option (%s) not available. %s' % (test_job,e) )


doSelectionZ=False
doSelectionMC=False
doSelectionFake=False
doSelectionJpsi=False

if 'Selection' in dir():
  if Selection == 'Zee':
    doSelectionZ = True
    Mass = [80. , 100.]
    OfflineTagMinEt=26
    OfflineProbeMinEt=4
  elif Selection == 'Fake':
    doSelectionFake = True
    Mass = [80. , 100.]
    OfflineTagMinEt=26
    OfflineProbeMinEt=4
  elif Selection == 'Jpsiee':
    Mass = [2.8 , 3.3]
    #Mass = [0 , 20.0]
    #Mass = [0 , 35.0]
    OfflineTagMinEt=4.5
    OfflineProbeMinEt=4.5
    doSelectionJpsi=True
    # check if this is jpsiee monitoring look into the trigger tag list
    goodJpsiTagList = False
    for triItem in _monitoringTP_tags:
      if 'Jpsi' in trigItem:
        goodJpsieTaglist = True
        break
    if not goodJpsiTagList:
      print _monitoringTP_tags
      raise RuntimeError("Jpsiee tag list must be into the trigger list")
  else:
    print 'Option (%s) not available' % Selection
else:
  print 'Using default collection parameters'
  doSelectionZ=True
  Mass = [80. , 100.]
  OfflineTagMinEt=26
  OfflineProbeMinEt=4



print _monitoringTP_tags
print _monitoringTP



####################################################################################################

DetailedHistograms=False
acf.FilesInput=finallist
acf.EvtMax=nov
rec.readAOD=True
# switch off detectors
rec.doForwardDet=False
rec.doInDet=False
rec.doCalo=False
rec.doMuon=False
rec.doEgamma=False
rec.doTrigger=True
recAlgs.doTrigger=False # disable trigger (maybe necessary if detectors switched off)
rec.doMuon=False
rec.doMuonCombined=False
rec.doWriteAOD=True
rec.doWriteESD=False
rec.doDPD=False
rec.doTruth=False
# autoconfiguration might trigger undesired feature
rec.doESD.set_Value_and_Lock(False)      # uncomment if do not run ESD making algorithms
rec.doWriteESD.set_Value_and_Lock(False) # uncomment if do not write ESD
rec.doAOD.set_Value_and_Lock(True)       # uncomment if do not run AOD making algorithms
rec.doWriteAOD.set_Value_and_Lock(False) # uncomment if do not write AOD
rec.doWriteTAG.set_Value_and_Lock(False) # uncomment if do not write TAG

# main jobOption
include ("RecExCommon/RecExCommon_topOptions.py")
MessageSvc.debugLimit = 20000000
MessageSvc.infoLimit  = 20000000

# Add Trigger Decision Tool
from TrigDecisionTool.TrigDecisionToolConf import Trig__TrigDecisionTool
tdt = Trig__TrigDecisionTool( "TrigDecisionTool" )
tdt.TrigDecisionKey='xTrigDecision'
tdt.Navigation.ReadonlyHolders=True
ToolSvc+=tdt

try:
  include ("AthenaMonitoring/DataQualityInit_jobOptions.py")
except Exception:
  treatException("Could not load AthenaMonitoring/DataQualityInit_jobOptions.py")




# Call top sequence
from AthenaCommon.AlgSequence import AlgSequence, AthSequencer
topSequence = AlgSequence()


##################################### GRL Tools ##########################################
# Good Run List (GRL)
if doGRL:
  # update and fix the path
  from TrigEgammaJobConfigs.utilities import FindCalibPath
  for idx, grl in enumerate(grllist):
    grllist[idx] = FindCalibPath(grl)

  from RecExConfig.InputFilePeeker import inputFileSummary
  try:
    if inputFileSummary['evt_type'][0] == "IS_DATA":
      from GoodRunsLists.GoodRunsListsConf import *
      ToolSvc += GoodRunsListSelectorTool()
      GoodRunsListSelectorTool.GoodRunsListVec = grllist
      from GoodRunsListsUser.GoodRunsListsUserConf import *
      seq = AthSequencer("AthFilterSeq")
      seq += GRLTriggerSelectorAlg('GRLTriggerAlg1')
      seq.GRLTriggerAlg1.GoodRunsListArray = ['PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim']
      #seq.GRLTriggerAlg1.GoodRunsListArray = [grllist[0].replace('.xml','')]
  except:
    print "GRL not available!"

##################################### Mon Tools ##########################################

from AthenaMonitoring.AthenaMonitoringConf import AthenaMonManager
topSequence += AthenaMonManager( "HLTMonManager")
HLTMonManager = topSequence.HLTMonManager
################ Mon Tools #################
#Global HLTMonTool
from TrigHLTMonitoring.TrigHLTMonitoringConf import HLTMonTool
HLTMon = HLTMonTool(name  = 'HLTMon', histoPathBase = "HLT");
ToolSvc += HLTMon;
HLTMonManager.AthenaMonTools += [ "HLTMonTool/HLTMon" ];


####################################################################################################
#Define the base path for all histograms
from TriggerJobOpts.TriggerFlags import TriggerFlags


# Force this emulation selector path
if 'pidVersion' in dir():
  print 'Setting pidVersion as: ',pidVersion
  TriggerFlags.EgammaSlice.pidVersion.set_Value_and_Lock(pidVersion)
else:
  #if runFlag == 2017:
  if runFlag == '2017':
    #TriggerFlags.EgammaSlice.pidVersion.set_Value_and_Lock("ElectronPhotonSelectorTools/trigger/rel21_20180216/")
    TriggerFlags.EgammaSlice.pidVersion.set_Value_and_Lock("ElectronPhotonSelectorTools/trigger/rel21_20170217/")
  elif runFlag == '2018':
    TriggerFlags.EgammaSlice.pidVersion.set_Value_and_Lock("ElectronPhotonSelectorTools/trigger/rel21_20180312/")
  else:
    TriggerFlags.EgammaSlice.pidVersion.set_Value_and_Lock("ElectronPhotonSelectorTools/trigger/rel21_20180312/")


from TriggerMenu.egamma.EgammaSliceFlags import EgammaSliceFlags
EgammaSliceFlags.ringerVersion.set_On()

if 'ringerVersion' in dir():
  print 'Setting ringerVersion as: ',ringerVersion
  TriggerFlags.EgammaSlice.ringerVersion.set_Value_and_Lock(ringerVersion)
else:
  if runFlag == '2017':
    TriggerFlags.EgammaSlice.ringerVersion.set_Value_and_Lock("RingerSelectorTools/TrigL2_20170505_v6")
  elif runFlag == '2018':
    TriggerFlags.EgammaSlice.ringerVersion.set_Value_and_Lock("RingerSelectorTools/TrigL2_20180125_v8")
  else:
    TriggerFlags.EgammaSlice.ringerVersion.set_Value_and_Lock("RingerSelectorTools/TrigL2_20180125_v8")


####################################################################################################


basePath = '/HLT/Egamma'
#Configure the TrigEgammaPlotTool
from TrigEgammaAnalysisTools.TrigEgammaProbelist           import monitoring_mam, monitoring_electron, monitoring_photon
from TrigEgammaAnalysisTools.TrigEgammaProbelist           import probeListLowMidPtSupportingTriggers, probeListHighPtSupportingTriggers, supportingTriggerList
from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConfig import TrigEgammaNavTPAnalysisTool, TrigEgammaNavTPJpsieeAnalysisTool,\
    TrigEgammaPlotTool, EfficiencyTool, DistTool, ResolutionTool

PlotTool = TrigEgammaPlotTool.copy( name="TrigEgammaPlotTool",
                                    DirectoryPath=basePath,
                                    MaM=monitoring_mam,
                                    Efficiency=[],
                                    Distribution=[],
                                    Resolution=[])

EffTool = EfficiencyTool.copy(name="EfficiencyTool",
                              PlotTool=PlotTool,
                              isEMResultNames=["Tight","Medium","Loose"],
                              LHResultNames=["LHTight","LHMedium","LHLoose"],
                              OutputLevel=0)


####################################################################################################
from TrigEgammaEmulationTool.TrigEgammaEmulationToolConfig import TrigEgammaEmulationTool
from TrigEgammaEmulationTool.TrigEgammaEmulationEFConfig   import EgammaEFElectronDefaultEmulator,EgammaEFElectronNoD0Emulator


EmulationElectronTool = TrigEgammaEmulationTool.copy( name="TrigEgammaEmulationTool",
                                                      TriggerList = _monitoringTP_tags+_monitoringTP,
                                                      #TriggerList = [],
                                                      SupportingTriggerList=supportingTriggerList,
                                                      #SupportingTriggerList='HLT_e*',
                                                      DoL2ElectronFex=True,
                                                      DoEFCaloPid=False,
                                                      DoRinger= True,
                                                      DoRingerBelow15GeV=False,
                                                      OutputLevel=1)

####################################################################################################

####################################################################################################
from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConfig import getEventSelectionTool

TrigEgammaEventSelection = getEventSelectionTool( runFlag )
Tool = TrigEgammaEventSelection( name='EventSelection',
                              Analysis='Jpsi',
                              PlotTool=PlotTool,
                              EmulationTool=EmulationElectronTool,
                              File="",
                              OutputLevel=1,
                              DetailedHistograms=DetailedHistograms,
                              isEMResultNames=["Tight","Medium","Loose"],
                              LHResultNames=["LHTight","LHMedium","LHLoose"],
                              ZeeLowerMass=Mass[0],
                              ZeeUpperMass=Mass[1],
                              OfflineTagSelector='LHTight', # 1=tight, 2=medium, 3=loose
                              OfflineProbeSelector='LHLoose',
                              ForceProbePid=False,
                              RemoveCrack=False,
                              OppositeCharge=True,
                              OfflineTagMinEt=OfflineTagMinEt,
                              OfflineProbeMinEt=OfflineProbeMinEt,
                              TagTriggerList=_monitoringTP_tags,
                              SelectionZ=doSelectionZ,
                              SelectionMC=doSelectionMC,
                              SelectionJpsi=doSelectionJpsi,
                              SelectionFakes=doSelectionFake,
                              ApplyMinimalTrigger=True,
                              DetailedDataLevel=2,
                              DumpTags=False,
                              TriggerList=_monitoringTP,
                              # Jpsi special cuts
                              DoJpsiTagTrackIsolation  = True,
                              DoJpsiTagTighterTRT      = False,
                              DoJpsiProbeTRTCut        = False,
                              DoJpsiProbeSiCut         = True,
                              JpsiTriggers             = _monitoringTP_tags ,  # shold be jpsiee in the name
                              )

####################################################################################################
Tools=['TrigEgammaEventSelection/EventSelection']
from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConf import TrigEgammaMonTool
TrigEgammaMonTool = TrigEgammaMonTool( name = "HLTEgammaMon", histoPathBase=basePath, Tools=Tools)
ToolSvc += TrigEgammaMonTool
HLTMonManager.AthenaMonTools += [ "TrigEgammaMonTool/HLTEgammaMon" ]
HLTMonManager.FileKey = "GLOBAL"
ToolSvc.TrigDecisionTool.Navigation.OutputLevel = WARNING
####################################################################################################







