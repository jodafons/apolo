/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include <iostream>
#include "TrigMultiVarHypo/tools/ConvMultiLayerPerceptron.h"
#include <algorithm>
#include <math.h>

//!***************************************************************

Conv2D::Conv2D( REAL bias, std::vector<std::vector<REAL>> &kernel, unsigned ki, unsigned kj, unsigned ii, unsigned ij, std::string &TF):
        
        m_bias(bias),
        m_kernel(kernel),
        m_kernel_i(ki), 
        m_kernel_j(kj),
        m_input_i(ii),
        m_input_j(ij), 
        m_tfname(TF)
{


  m_output_i = ii-ki+1;
  m_output_j = ij-kj+1;
  // allocate space to the output matrix
  try{
    // Second dimension of weights
    m_output = new REAL*[m_output_i];
    for(unsigned i=0; i < (m_output_i); i++){
      m_output[i] = new REAL [m_output_j];
    }
  } catch (std::bad_alloc xa){
    throw;
  }


}

//!***************************************************************

Conv2D::~Conv2D(){
  release();
}

//!***************************************************************

void Conv2D::release()
{  
  if(m_output){
    for(unsigned i=0; i < (m_output_i); i++){
      if(m_output[i])  delete[] m_output[i];
    }
    delete[] m_output;
  }
}

//!***************************************************************

void Conv2D::propagate( std::vector<REAL**> &input)
{
  // clear all parameters
  reset();
  
  for(unsigned i=0; i< input.size(); ++i){
    for( unsigned oi=0; oi < m_output_i; ++oi){
      for( unsigned oj=0; oj < m_output_j; ++oj){
        for( unsigned ki=0; ki < m_kernel_i; ++ki){
          for( unsigned kj=0; kj < m_kernel_j; ++kj){
            m_output[oi][oj] += m_kernel[i][ ki*m_kernel_i + kj  ] * input[i][oi+ki][oj+kj];
          }// loop over kernel
        }// loop over kernel
      }// loop over output
    }// loop over output
  }// loop over all inputs


  //bias();
  // apply transfer function
  activation(); // For all positions from the output matrix

}

//!***************************************************************

void Conv2D::reset(){
  for(unsigned i=0; i < m_output_i; ++i ){
    for(unsigned j=0; j < m_output_j; ++j )
      m_output[i][j] = m_bias;
  }
}

//!***************************************************************

void Conv2D::activation(){

  if( m_tfname == "linear" ) return;
  
  if( m_tfname == "relu"){
    for(unsigned i=0; i < m_output_i; ++i ){
      for(unsigned j=0; j < m_output_j; ++j )
        m_output[i][j] = m_output[i][j] > 0 ? m_output[i][j] : 0;
    }
  }
  
  if( m_tfname == "tanh" ){
    for(unsigned i=0; i < m_output_i; ++i ){
      for(unsigned j=0; j < m_output_j; ++j )
        m_output[i][j] = std::tanh(m_output[i][j]);
    }
  }
}

//!***************************************************************
//!***************************************************************
//!***************************************************************
//!***************************************************************
//!***************************************************************
//!***************************************************************



ConvMultiLayerPerceptron::ConvMultiLayerPerceptron( 
     // dense layer
     std::vector<unsigned int>  &dense_n,  
     std::vector<REAL>          &dense_w,  
     std::vector<REAL>          &dense_b,
     std::vector<std::string>   &dense_TF,
     // convolutional layer
     std::vector<unsigned int>  &conv_frame,
     unsigned int conv_input_i,
     unsigned int conv_input_j,
     std::vector<unsigned int>  &conv_n,
     std::vector<unsigned int>  &conv_kernel_i,
     std::vector<unsigned int>  &conv_kernel_j,
     std::vector<REAL>           conv_kernel, // must be a copy
     std::vector<REAL>           conv_bias, // must be a copy
     std::vector<std::string>   &conv_TF,
     // model parameters
     REAL etmin, 
     REAL etmax, 
     REAL etamin, 
     REAL etamax, 
     REAL mumin, 
     REAL mumax
     ): 
  Model(etmin,etmax,etamin,etamax,mumin,mumax), 
  m_input_i(conv_input_i), 
  m_input_j(conv_input_j),
  m_kernel_i(conv_kernel_i),
  m_kernel_j(conv_kernel_j),
  m_frame(conv_frame)
{



  // allocate space to the output matrix
  try{
    // Second dimension of weights
    m_rings = new REAL*[m_input_i];
    for(unsigned i=0; i < m_input_i; i++){
      m_rings[i] = new REAL [m_input_j];
    }
  } catch (std::bad_alloc xa){
    throw;
  }

  try{
    // create the multi layer perceptron object (dense, fully connected)
    m_dense = new MultiLayerPerceptron( dense_n, dense_w, dense_b, dense_TF,
                                        etmin,etmax,etamin,
                                        etamax,mumin,mumax);
  }catch (std::bad_alloc xa){
    throw;
  }


  std::reverse(conv_kernel.begin(), conv_kernel.end());
  std::reverse(conv_bias.begin(), conv_bias.end());
  
  unsigned ii = m_input_i;
  unsigned ij = m_input_j;

  for(unsigned l=0; l < conv_n.size(); ++l){
    
    std::vector<Conv2D*> layer;

    for(unsigned n=0; n < conv_n.at(l); ++n){

      // get the kernel values
      std::vector<std::vector<REAL>> kernel_units;
      unsigned units = l > 0? conv_n.at(l-1) : 1;

      for(unsigned u=0; u<units;++u){
        std::vector<REAL> kernel;
        for( unsigned k=0; k < (conv_kernel_i[l] * conv_kernel_j[l]); ++k){
          kernel.push_back( conv_kernel.back() );
          conv_kernel.pop_back();
        }
        kernel_units.push_back(kernel);
      }

      try{// Attach convolutional unit
        layer.push_back( new Conv2D( conv_bias.back(), kernel_units, conv_kernel_i[l], conv_kernel_j[l], ii, ij ,conv_TF[l] ) );
        
        conv_bias.pop_back();
      } catch (std::bad_alloc xa){
         throw;
      }


    }// loop over conv2D neurons

    m_convLayers.push_back( layer );
    ii -= conv_kernel_i[l]; ++ii;
    ij -= conv_kernel_j[l]; ++ij;
  }// loop over layers
  


}


//!***************************************************************

ConvMultiLayerPerceptron::~ConvMultiLayerPerceptron()
{
  release();
  for(unsigned l=0; l < m_convLayers.size(); ++l){
    for(unsigned n=0; n < m_convLayers[l].size(); ++n)
      delete m_convLayers[l][n];
  }
  m_convLayers.clear();
  delete m_dense;
}

//!***************************************************************

void ConvMultiLayerPerceptron::release()
{  
  if(m_rings){
    for(unsigned i=0; i < m_input_i; i++){
      if(m_rings[i])  delete[] m_rings[i];
    }
    delete[] m_rings;
  }
}
//!***************************************************************

std::vector<float> ConvMultiLayerPerceptron::flatten( std::vector<REAL**> &inputs, unsigned input_i, unsigned input_j ){

  std::vector<float> input_flatten;

  for(unsigned i=0; i < input_i; ++i){
    for(unsigned j=0; j < input_j; ++j)
      for(unsigned n=0; n<inputs.size(); ++n){
        input_flatten.push_back( (float)inputs.at(n)[i][j] );
    }
  }// Loop over inputs
  return input_flatten;
}


//!***************************************************************

float ConvMultiLayerPerceptron::propagate( std::vector<float> &rings ){

  reshape2vortex(rings);

  std::vector<REAL**> input;
  unsigned ii = m_input_i;
  unsigned ij = m_input_j;


  // propagate the input throut the convolutional layer
  for( unsigned l=0; l< m_convLayers.size(); ++l){

    if( l > 0 ){
      std::vector<REAL**> inputs;
      // get all previsly output pointers
      for(unsigned ll=0; ll<m_convLayers[l-1].size(); ++ll)
        inputs.push_back(m_convLayers[l-1][ll]->getOutput());
      // propagate in all neurons   
      for(unsigned ll=0; ll<m_convLayers[l].size(); ++ll)
        m_convLayers[l][ll]->propagate(inputs);

    }else{// propagate the first layer
      std::vector<REAL**> inputs;
      inputs.push_back(m_rings);
      for(unsigned ll=0; ll<m_convLayers[l].size(); ++ll){
        m_convLayers[l][ll]->propagate(inputs);
      }
    }
    ii -= m_kernel_i[l];
    ij -= m_kernel_j[l];
    ++ii; ++ij;


  }
  
  // After propagate all frames we need to flatter the last output
  // from the convolutional layer
  auto iend = m_convLayers.size()-1;
  std::vector<REAL**> inputs;
  for(unsigned ll=0; ll<m_convLayers[iend].size(); ++ll)
    inputs.push_back( m_convLayers[iend][ll]->getOutput() );

  // Flatten all convolutional outputs (iiXij)
  auto input_flatten = flatten(inputs, ii, ij);

  setOutput(m_dense->propagate(input_flatten));
  setOutputBeforeTheActivationFunction( m_dense->getOutputBeforeTheActivationFunction()) ;
  
  return getOutput();
}
//!***************************************************************
/*
const unsigned vortex[] = { 72,73,74,75,76,77,78,79,80,81,
                            71,42,43,44,45,46,47,48,49,82,
                            70,41,20,21,22,23,24,25,50,83,
                            69,40,19,6 ,7 ,8 ,9 ,26,51,84,
                            68,39,18,5 ,0 ,1 ,10,27,52,85,
                            67,38,17,4 ,3 ,2 ,11,28,53,86,
                            66,37,16,15,14,13,12,29,54,87,
                            65,36,35,34,33,32,31,30,55,88,
                            64,63,62,61,60,59,58,57,56,89,
                            99,98,97,96,95,94,93,92,91,90};
*/

void ConvMultiLayerPerceptron::reshape2vortex( std::vector<float> &rings )
{
  for(unsigned i=0; i<m_input_i; ++i){
    for(unsigned j=0; j<m_input_j; ++j){
      //m_rings[j][i] = (REAL)rings.at( vortex[i*10 + j] );
      m_rings[i][j] = (REAL)rings.at( m_frame[i*m_input_j + j] );
      //m_rings[i][j] = (REAL)rings.at( i*m_input_i + j );
    }
  }
}


//!***************************************************************







