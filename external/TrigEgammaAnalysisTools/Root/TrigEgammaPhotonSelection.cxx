/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/
/**********************************************************************
 * AsgTool: TrigEgammaPhotonSelection
 * Authors:
 *      João Victor da Fonseca Pinto <joao.victor.da.fonseca.pinto@cern.ch>
 *      Juan Lieber Marin <juan.lieber.marin@cern.ch>
 * Date: July 2020
 * Description:
 *      Derived class for dumping the MC photon/jets information into a ttree.
 *     
 **********************************************************************/

#include "TrigEgammaAnalysisTools/TrigEgammaPhotonSelection.h"
#include <TTree.h>
using namespace std;

TrigEgammaPhotonSelection::TrigEgammaPhotonSelection( const std::string& myname):
  TrigEgammaNavTPBaseTool(myname) , TrigEgammaPhysValBase()
{
	declareProperty("SupportTriggerList"        , m_supportList               );
	declareProperty("SelectionMC"               , m_selectionMC               );
	declareProperty("FakeSelection"             , m_fakeSelection             );
	declareProperty("EFCaloPhotonIsEMSelector"  ,	m_EFCaloPhotonIsEMSelectors	);
 	declareProperty("HLTPhotonIsEMSelector"     , m_HLTPhotonIsEMSelectors	  );
}

TrigEgammaPhotonSelection :: ~TrigEgammaPhotonSelection () {
}


StatusCode TrigEgammaPhotonSelection::childBook(){

  ATH_MSG_DEBUG("Now configuring chains for analysis");
  //Set the base directory from the plot()
  m_dir=plot()->getBasePath();
  ATH_MSG_INFO("Base Path: " << m_dir);
  std::vector<std::string> chains  = tdt()->getListOfTriggers("HLT_e.*, L1_EM.*, HLT_g.*");

  for(const auto trigName:m_trigInputList){ 
    if (std::find(chains.begin(), chains.end(), trigName) != chains.end()){ 
      if(plot()->getTrigInfoMap().count(trigName) != 0)
        ATH_MSG_WARNING("Trigger already booked, removing from trigger list " << trigName);
      else{ 
        m_trigList.push_back(trigName);
        setTrigInfo(trigName);
      }
    }// emulation 
  }
 
  addDirectory(m_dir+"/Egamma");

  // create the TDT metadata for future access
  TTree *tdtMetadata = new TTree("tdt", "Trigger  metadata navigation");
	createTDTMetadata( tdtMetadata, m_supportList );
	addTree(tdtMetadata, m_dir+"/Egamma");
	
	
  if (m_fakeSelection){
		TTree *fakes = new TTree("fakes", "tree of fakes photons");
		bookEventBranches ( fakes );
		bookPhotonBranches( fakes );
		bookTriggerPhotonBranches ( fakes );
		bookMonteCarloBranches ( fakes );
		addTree(fakes, m_dir + "/Egamma");
	}else{
	  TTree *photons = new TTree( "photons", "tree of photons");
    bookEventBranches( photons );
	  bookPhotonBranches( photons );
	  bookTriggerPhotonBranches ( photons );
    bookMonteCarloBranches( photons );
    addTree(photons, m_dir+"/Egamma");
  }
	
	if(!m_storeGate->contains<xAOD::CaloRingsContainer>("PhotonCaloRings")){
		ATH_MSG_INFO("No PhotonCaloRings container available");
		ATH_MSG_DEBUG("Offline electron calo rings in SG");
		m_doCaloRings=true;
	}else{
    m_doCaloRings=false;
  }
  ///Alloc pointers
  alloc_space();
  return StatusCode::SUCCESS;
}


StatusCode TrigEgammaPhotonSelection::childInitialize(){
  
  if ( (m_HLTPhotonIsEMSelectors.retrieve()).isFailure() ){
     ATH_MSG_ERROR( "Could not retrieve HLT IsEM Selector Tool! Can't work");
     return StatusCode::FAILURE;
  }
  if ( (m_EFCaloPhotonIsEMSelectors.retrieve()).isFailure() ){
     ATH_MSG_ERROR( "Could not retrieve EFCalo IsEM Selector Tool! Can't work");
     return StatusCode::FAILURE;
  }
	
  return StatusCode::SUCCESS;
}


StatusCode TrigEgammaPhotonSelection::childExecute(){

  m_eventCounter++;

  cd(m_dir+"/Expert/Event");
	if(!m_selectionMC) EventSelectionReal();
	else EventSelectionMC();

  return StatusCode::SUCCESS;
}


bool TrigEgammaPhotonSelection :: EventSelectionReal(){
	if (m_fakeSelection){
		//Insert fake selection from real data (Zrad mass matching and loose criteria analysis)
	}
	else{
		//Insert photon selection from real data (Zrad mass matching and tight criteria analysis)
	}
	return true;
}
	

bool TrigEgammaPhotonSelection::EventSelectionMC(){
	cd(m_dir+"/Egamma");
  	const xAOD::PhotonContainer* photons = nullptr;
	evtStore()->retrieve(photons,"Photons");
	for (const xAOD::Photon *photon : *photons){
		const xAOD::TruthParticle* true_photon = xAOD::TruthHelpers::getTruthParticle(*photon);
		if (m_fakeSelection){
			if (!true_photon){
				fill(tree("fakes", m_dir+"/Egamma"), photon); //Fill a fake MC photon
			 	continue;
			}
		}
		if(true_photon){
			fill(tree("photons"  , m_dir+"/Egamma"), photon); //Fill a true MC photon
			continue;
		}
	
	}
	return true;
}

bool TrigEgammaPhotonSelection::fill( TTree *t, const xAOD::Photon *ph){
 

  linkEventBranches(t); 
  linkPhotonBranches(t); 
  linkTriggerPhotonBranches(t);
  linkMonteCarloBranches(t); 

  const HLT::TriggerElement* feat=nullptr;

  if(!Match( ph,  feat )) {
    ATH_MSG_INFO("Impossible to match!");
    return false;
  }

  if(feat){
    clear();
    fillEvent();
    fillPhoton( ph );
    if(!fillMonteCarlo( ph ) ){
      ATH_MSG_WARNING("Cound not found any TruthParticle for this Electron");
    }
 
    ///Start trigger analisys...
    const xAOD::EmTauRoI *emTauRoI = getFeature<xAOD::EmTauRoI>(feat);
    if(emTauRoI){
      fillEmTauRoI( emTauRoI ); 
    }else{ 
      ATH_MSG_WARNING("Cound not found EmTauRoI in this TriggerElement..");
    }
  
    const xAOD::TrigEMCluster *emCluster = getFeature<xAOD::TrigEMCluster>(feat);
    if(emCluster){
      if(!fillTrigEMCluster( emCluster )){
        ATH_MSG_WARNING("Cound not attach the trigEMCluster information into the tree.");
      }

      if(!fillTrigCaloRings( feat )){
        ATH_MSG_WARNING("Cound not attach the TrigRingerRings information into the tree.");
      }
    }///cluster protection
  
  
    const xAOD::CaloClusterContainer *caloCont = getFeature<xAOD::CaloClusterContainer>(feat);
    if(caloCont){
      for(const auto& cl : *caloCont){
        if(!fillCaloCluster(cl)){
          ATH_MSG_WARNING("Cound not attach the CaloCluster information into the tree.");
        }
      }// loop over calo cluster
    }
  
    /*
    const xAOD::TrigPhotonContainer *trigPhCont = getFeature<xAOD::TrigPhotonContainer>(feat); 

    if(trigPhCont){
      for(const auto& trigPh : *trigPhCont){
        if(!fillTrigPhoton(trigPh)) {
          ATH_MSG_WARNING("Cound not attach the trigPhoton information into the tree.");
        }
      }// loop over all trigPhoton for this feat
      count("Fill_TrigPhoton");
    }*/

    const xAOD::PhotonContainer *phCont = getFeature<xAOD::PhotonContainer>(feat);

    if(phCont){
      for(const auto& hlt_ph : *phCont){
        if(!fillHLTPhoton(hlt_ph))
          ATH_MSG_WARNING("Cound not attach the HLT Photon information into the tree.");
      }
   }

    fillTDT( ph );


    ATH_MSG_INFO("recording trigger information into the file.");
    t->Fill();
  }//loop over trigger objects

  return true;
}



bool TrigEgammaPhotonSelection :: Match (const xAOD::Egamma *eg, const HLT::TriggerElement *&feat) {
  ATH_MSG_INFO("Try to match....");
    // const xAOD::TruthParticle* true_photon = xAOD::TruthHelpers::getTruthParticle(*photon);
  for (const auto& chain : m_supportList ){
	const HLT::TriggerElement *te = nullptr;
	if (!match()->match(eg,chain,te)) continue;
	const xAOD::PhotonContainer *trigPhContainer = getFeature<xAOD::PhotonContainer>(te);
	if (trigPhContainer){
		feat = te;
		return true;
	} 
  }
  return false;
}



bool TrigEgammaPhotonSelection::fillTDT(const xAOD::Photon *ph ){

  ATH_MSG_DEBUG("FillTDT...");
  for(auto& trigItem : m_supportList){
    const HLT::TriggerElement* feat=nullptr;
    match()->match(ph, trigItem, feat);
    if(feat){
      setAccept(feat, getTrigInfo(trigItem));
      bool passedL1Calo  = getAccept().getCutResult("L1Calo");
      bool passedL2Calo  = getAccept().getCutResult("L2Calo");
      bool passedL2      = getAccept().getCutResult("L2");
      bool passedEFCalo  = getAccept().getCutResult("EFCalo");
      bool passedHLT     = getAccept().getCutResult("HLT");
      m_trig_tdt_L1_calo_accept->push_back( int(passedL1Calo) );
      m_trig_tdt_L2_calo_accept->push_back( int(passedL2Calo) );
      m_trig_tdt_L2_ph_accept->push_back( int(passedL2) );
      m_trig_tdt_EF_calo_accept->push_back( int(passedEFCalo) );
      m_trig_tdt_EF_ph_accept->push_back( int(passedHLT) );
      //m_trig_tdt_emu_decision_mask
      count("tdt_"+trigItem+"_total");
      if(passedL1Calo)  count("tdt_"+trigItem+"_L1Calo");
      if(passedL2Calo)  count("tdt_"+trigItem+"_L2Calo");
      if(passedL2)      count("tdt_"+trigItem+"_L2"    );
      if(passedEFCalo)  count("tdt_"+trigItem+"_EFCalo");
      if(passedHLT)     count("tdt_"+trigItem+"_HLT"   );
 
    }else{
      m_trig_tdt_L1_calo_accept->push_back( -1 );
      m_trig_tdt_L2_calo_accept->push_back( -1 );
      m_trig_tdt_L2_ph_accept->push_back( -1 );
      m_trig_tdt_EF_calo_accept->push_back( -1 );
      m_trig_tdt_EF_ph_accept->push_back( -1 );
      //ATH_MSG_DEBUG("Trigger bitmask was converted to " << int(GetByteFromBools(mask)));
      //m_trig_tdt_decision_mask->push_back(GetByteFromBools(mask));
    }

  }// Loop over triggers


  return true;

}

bool TrigEgammaPhotonSelection::fillTrigPhoton( const xAOD::TrigPhoton *trigPh ){
   ATH_MSG_INFO("FillTrigPhoton...");
   m_trig_L2_ph_caloEta     ->push_back(trigPh->caloEta());
   m_trig_L2_ph_pt          ->push_back(trigPh->pt());
   m_trig_L2_ph_phi         ->push_back(trigPh->phi());
   m_trig_L2_ph_eta         ->push_back(trigPh->eta());
   return true;
}

bool TrigEgammaPhotonSelection::fillHLTPhoton( const xAOD::Photon *ph ){

  float   calo_e(0);
  float   calo_et(0);
  float   calo_eta(0);
  float   calo_phi(0);
  float   calo_etaBE2(0);
  float   eta(0);
  float   et(0);
  //float   eta2(0);
  float   phi(0);
  float   ethad1(0);
  float   ehad1(0);
  float   f1(0);
  float   f3(0);
  float   f1core(0);
  float   f3core(0);
  float   weta1(0);
  float   weta2(0);
  float   wtots1(0);
  float   fracs1(0);
  float   Reta(0);
  float   Rphi(0);
  float   Eratio(0);
  float   Rhad(0);
  float   Rhad1(0);
  float   deta1(0);
  float   deta2(0);
  float   dphi2(0);
  float   dphiresc(0);
  float   deltaPhiRescaled2(0);
  float   e(0);
  float   e277(0);
  float   deltaE(0);
  bool hasCalo(false);


  
  ///Cluster information
  e                    = ph->e();
  et                   = ph->pt();
  eta                  = ph->eta();
  phi                  = ph->phi();

  ph->showerShapeValue( ethad1   , xAOD::EgammaParameters::ShowerShapeType::ethad1   ); 
  ph->showerShapeValue( ehad1    , xAOD::EgammaParameters::ShowerShapeType::ehad1    );
  ph->showerShapeValue( f1       , xAOD::EgammaParameters::ShowerShapeType::f1       ); // LH, isEM
  ph->showerShapeValue( f3       , xAOD::EgammaParameters::ShowerShapeType::f3       ); // LH, isEM
  ph->showerShapeValue( f1core   , xAOD::EgammaParameters::ShowerShapeType::f1core   );
  ph->showerShapeValue( f3core   , xAOD::EgammaParameters::ShowerShapeType::f3core   ); // isEM
  ph->showerShapeValue( weta1    , xAOD::EgammaParameters::ShowerShapeType::weta1    ); // LH, isEM (new)
  ph->showerShapeValue( weta2    , xAOD::EgammaParameters::ShowerShapeType::weta2    ); // LH, isEM
  ph->showerShapeValue( wtots1   , xAOD::EgammaParameters::ShowerShapeType::wtots1   ); // LH, isEM
  ph->showerShapeValue( fracs1   , xAOD::EgammaParameters::ShowerShapeType::fracs1   ); // isEM
  ph->showerShapeValue( Reta     , xAOD::EgammaParameters::ShowerShapeType::Reta     ); // LH, isEM
  ph->showerShapeValue( Rphi     , xAOD::EgammaParameters::ShowerShapeType::Rphi     ); // LH, isEM
  ph->showerShapeValue( Eratio   , xAOD::EgammaParameters::ShowerShapeType::Eratio   ); // LH, isEM
  ph->showerShapeValue( Rhad     , xAOD::EgammaParameters::ShowerShapeType::Rhad     ); // LH, isEM
  ph->showerShapeValue( Rhad1    , xAOD::EgammaParameters::ShowerShapeType::Rhad1    ); // LH, isEM
  ph->showerShapeValue( e277     , xAOD::EgammaParameters::e277                      ); // isEM (new)
  ph->showerShapeValue( deltaE   , xAOD::EgammaParameters::DeltaE                    ); // isEM (new)
  ///Combined Vertex/Cluter information
  // ph->vertexCaloMatchValue( deta2     , xAOD::EgammaParameters::VertexCaloMatchType::deltaEta2          );
  // ph->vertexCaloMatchValue( dphi2     , xAOD::EgammaParameters::VertexCaloMatchType::deltaPhi2          );
  // ph->vertexCaloMatchValue( dphiresc  , xAOD::EgammaParameters::VertexCaloMatchType::deltaPhiRescaled0  );
  // deltaPhiRescaled2  =  get_el_deltaPhiRescaled2(ph); // LH
  // deta1              =  get_el_deltaEta1(ph); // LH
 
  if(ph->caloCluster()){
    hasCalo=true;
    calo_et                 = getCluster_et( ph );
    calo_eta                = getCluster_eta( ph );
    calo_phi                = getCluster_phi( ph );
    calo_etaBE2             = ph->caloCluster()->etaBE(2); // LH
    calo_e                  = ph->caloCluster()->e(); // LH
  }
 


  ///Vertex particle information
  const xAOD::Vertex *vertex = ph->vertex();
  if(vertex){
    // hasTrack=true;
    // track->summaryValue( eprobht                      , xAOD::SummaryType::eProbabilityHT ); // LH
    // track->summaryValue( nblayerhits                  , xAOD::SummaryType::numberOfBLayerHits ); // LH, isEM
    // track->summaryValue( nblayerolhits                , xAOD::SummaryType::numberOfBLayerOutliers ); // LH, isEM
    // track->summaryValue( npixhits                     , xAOD::SummaryType::numberOfPixelHits ); // LH, isEM
    // track->summaryValue( npixolhits                   , xAOD::SummaryType::numberOfPixelOutliers ); // isEM
    // track->summaryValue( npixdeadsensors              , xAOD::SummaryType::numberOfPixelDeadSensors); // LH, isEM
    // track->summaryValue( nscthits                     , xAOD::SummaryType::numberOfSCTHits ); // LH, isEM
    // track->summaryValue( nsctolhits                   , xAOD::SummaryType::numberOfSCTOutliers ); // isEM
    // track->summaryValue( nsctdeadsensors              , xAOD::SummaryType::numberOfSCTDeadSensors); // isEM
    // track->summaryValue( ntrthits                     , xAOD::SummaryType::numberOfTRTHits); // isEM
    // track->summaryValue( ntrtolhits                   , xAOD::SummaryType::numberOfTRTOutliers ); // isEM
    // track->summaryValue( ntrthighthreshits            , xAOD::SummaryType::numberOfTRTHighThresholdHits ); // isEM
    // track->summaryValue( ntrthighthresolhits          , xAOD::SummaryType::numberOfTRTHighThresholdOutliers ); // isEM
    // track->summaryValue( ntrtxenonhits                , xAOD::SummaryType::numberOfTRTXenonHits ); // isEM
    // track->summaryValue( expectblayerhit              , xAOD::SummaryType::expectBLayerHit ); // LH, isEM
    // track->summaryValue( expectNextToInnerMostLayer   , xAOD::expectNextToInnermostPixelLayerHit); // LH, isEM
    // track->summaryValue( nNextToInnerMostLayerHits    , xAOD::numberOfNextToInnermostPixelLayerHits); // LH, isEM
    // track->summaryValue( nNextToInnerMostLayerOutliers, xAOD::numberOfNextToInnermostPixelLayerOutliers); // LH, isEM
   
    // pt                 =  track->pt();
    // trk_eta            =  track->eta(); // LH
    // charge             =  track->charge(); // LH
    // qOverP             =  track->qOverP();  // LH, isEM
    // sigd0              =  get_el_sigd0(el); // LH
    // d0                 =  get_el_d0(el); // LH, isEM
    // eProbabilityHT     =  get_el_eProbabilityHT(el); // LH, isEM
    // transformed_eProbabilityHT=get_el_transformed_eProbabilityHT(el); // LH
    // d0significance     =  get_el_d0significance(el); 
    // deltaPOverP        =  get_el_DeltaPOverP(el); // LH  
  }

  m_trig_EF_ph_hasCalo                      ->push_back( hasCalo ); 
  // m_trig_EF_el_hasTrack                     ->push_back( hasTrack );

  m_trig_EF_calo_tight                      ->push_back(static_cast<bool>(m_EFCaloPhotonIsEMSelectors[0]->accept(ph)));
  m_trig_EF_calo_medium                     ->push_back(static_cast<bool>(m_EFCaloPhotonIsEMSelectors[1]->accept(ph)));
  m_trig_EF_calo_loose                      ->push_back(static_cast<bool>(m_EFCaloPhotonIsEMSelectors[2]->accept(ph)));
  m_trig_EF_ph_tight                        ->push_back(static_cast<bool>(m_HLTPhotonIsEMSelectors[0]->accept(ph)));
  m_trig_EF_ph_medium                       ->push_back(static_cast<bool>(m_HLTPhotonIsEMSelectors[1]->accept(ph)));
  m_trig_EF_ph_loose                        ->push_back(static_cast<bool>(m_HLTPhotonIsEMSelectors[2]->accept(ph)));
  m_trig_EF_ph_calo_et                      ->push_back( calo_et); 
  m_trig_EF_ph_calo_eta                     ->push_back( calo_eta ); 
  m_trig_EF_ph_calo_phi                     ->push_back( calo_phi ); 
  m_trig_EF_ph_calo_etaBE2                  ->push_back( calo_etaBE2 ); 
  m_trig_EF_ph_calo_e                       ->push_back( calo_e ); 
  m_trig_EF_ph_et                           ->push_back( et );
  m_trig_EF_ph_eta                          ->push_back( eta );
  m_trig_EF_ph_phi                          ->push_back( phi );
  m_trig_EF_ph_e                            ->push_back( e );
  m_trig_EF_ph_e277                         ->push_back( e277 );
  m_trig_EF_ph_deltaE                       ->push_back( deltaE );
  m_trig_EF_ph_ethad1                       ->push_back( ethad1 );
  m_trig_EF_ph_ehad1                        ->push_back( ehad1 );
  m_trig_EF_ph_f1                           ->push_back( f1 );
  m_trig_EF_ph_f3                           ->push_back( f3 );
  m_trig_EF_ph_f1core                       ->push_back( f1core );
  m_trig_EF_ph_f3core                       ->push_back( f3core );
  m_trig_EF_ph_weta1                        ->push_back( weta1 );
  m_trig_EF_ph_weta2                        ->push_back( weta2 );
  m_trig_EF_ph_wtots1                       ->push_back( wtots1 );
  m_trig_EF_ph_fracs1                       ->push_back( fracs1 );
  m_trig_EF_ph_Reta                         ->push_back( Reta );
  m_trig_EF_ph_Rphi                         ->push_back( Rphi );
  m_trig_EF_ph_Eratio                       ->push_back( Eratio );
  m_trig_EF_ph_Rhad                         ->push_back( Rhad );
  m_trig_EF_ph_Rhad1                        ->push_back( Rhad1 );
  m_trig_EF_ph_deta2                        ->push_back( deta2 );
  m_trig_EF_ph_dphi2                        ->push_back( dphi2 );
  m_trig_EF_ph_dphiresc                     ->push_back( dphiresc );
  m_trig_EF_ph_deltaEta1                    ->push_back( deta1 );
  m_trig_EF_ph_deltaPhiRescaled2            ->push_back( deltaPhiRescaled2 );
  return true;
}

bool TrigEgammaPhotonSelection::fillEmTauRoI( const xAOD::EmTauRoI *emTauRoI ){
  
  ATH_MSG_DEBUG("FillEmTauRoI...");
  m_trig_L1_eta     = emTauRoI->eta();
  m_trig_L1_phi     = emTauRoI->phi();
  m_trig_L1_emClus  = emTauRoI->emClus();
  m_trig_L1_tauClus = emTauRoI->tauClus();
  m_trig_L1_emIsol  = emTauRoI->emIsol();
  m_trig_L1_hadIsol = emTauRoI->hadIsol();
  m_trig_L1_hadCore = emTauRoI->hadCore();
  return true;
} 

bool TrigEgammaPhotonSelection::fillTrigEMCluster( const xAOD::TrigEMCluster *emCluster ){
  
  ATH_MSG_DEBUG("FillTrigEMCluster...");
  m_trig_L2_calo_et         = emCluster->et();
  m_trig_L2_calo_eta        = emCluster->eta();
  m_trig_L2_calo_phi        = emCluster->phi();
  m_trig_L2_calo_emaxs1     = emCluster->emaxs1();
  m_trig_L2_calo_e2tsts1    = emCluster->e2tsts1();
  m_trig_L2_calo_e237       = emCluster->e237();
  m_trig_L2_calo_e277       = emCluster->e277();
  m_trig_L2_calo_ehad1      = emCluster->ehad1();
  m_trig_L2_calo_weta2      = emCluster->weta2();
  m_trig_L2_calo_wstot      = emCluster->wstot();
  m_trig_L2_calo_fracs1     = emCluster->fracs1();
  for(unsigned i=0; i<emCluster->energySample().size(); ++i){
    m_trig_L2_calo_energySample->push_back( emCluster->energySample().at(i));
  }

  return true;
}

bool TrigEgammaPhotonSelection::fillCaloCluster( const xAOD::CaloCluster *cluster){

  ATH_MSG_DEBUG("FillCaloCluster...");
  m_trig_EF_calo_et->push_back(cluster->et());
  m_trig_EF_calo_eta->push_back(cluster->eta());
  m_trig_EF_calo_phi->push_back(cluster->phi());
  m_trig_EF_calo_etaBE2->push_back(cluster->etaBE(2));
  m_trig_EF_calo_e->push_back(cluster->e());

  return true;
}

bool TrigEgammaPhotonSelection::fillEvent(){
  ///Event information
  if ( (m_storeGate->retrieve(m_eventInfo, "EventInfo")).isFailure() ){
        ATH_MSG_WARNING("Failed to retrieve eventInfo ");
        return false;
    }
  m_runNumber               = m_eventInfo->runNumber();
  m_eventNumber             = m_eventInfo->eventNumber();
  m_lumiBlock               = m_eventInfo->lumiBlock();
  m_avgmu                   = getAvgMu();
  return true;
}


bool TrigEgammaPhotonSelection::fillMonteCarlo(const xAOD::Photon *ph){
  if(m_storeGate->retrieve(m_truthContainer,"egammaTruthParticles").isFailure()){
        ATH_MSG_WARNING("Could not retrieve xAOD::TruthParticleContainer 'egammaTruthParticles'");
        return false;
  }
  if(m_truthContainer){
    const xAOD::TruthParticle* mc = xAOD::TruthHelpers::getTruthParticle(*ph);
    if(mc){
      m_mc_hasMC        = true;
      m_mc_pt           = mc->pt();
      m_mc_eta          = mc->eta();
      m_mc_phi          = mc->phi();
      m_mc_isTop        = mc->isTop();
      m_mc_isQuark      = mc->isQuark();
      m_mc_isParton     = mc->isParton();
      m_mc_isMeson      = mc->isMeson();
      m_mc_isTau        = mc->isTau();
      m_mc_isMuon       = mc->isMuon();
      m_mc_isPhoton     = mc->isPhoton();
      m_mc_isElectron   = mc->isElectron();
      return true;
    }//has match
  }//has truth container
  return false;
}



bool TrigEgammaPhotonSelection::fillTrigCaloRings( const HLT::TriggerElement *feat ){
  
  ATH_MSG_DEBUG("FillTrigCaloRings...");
  /* try to get the calo rings energy for this TE!*/
  const xAOD::TrigRingerRings *ringer = getFeature<xAOD::TrigRingerRings>(feat);
  if(ringer){// from this TE
    for(unsigned r=0; r<ringer->size(); ++r){
      m_trig_L2_calo_rings->push_back(ringer->rings()[r]);
    }
  }else{// from others TEs
    const xAOD::TrigEMCluster *emCluster = getFeature<xAOD::TrigEMCluster>(feat);
    if(emCluster){
      // try to match without any other cluster.
      TrigEgammaAnalysisBaseTool::getTrigCaloRings(emCluster, *m_trig_L2_calo_rings);
    }else{
      return false;
    }
  }
 
  return true;
}



bool TrigEgammaPhotonSelection::fillPhoton( const xAOD::Photon *ph ){

   ATH_MSG_DEBUG("FillPhoton...");
   float val(0);
   bool hasCalo(false);
   bool hasTrack(false);


  
   m_ph_e                    = ph->e();
   m_ph_et                   = ph->pt();
   m_ph_eta                  = ph->eta();
   m_ph_phi                  = ph->phi();

   ph->showerShapeValue( m_ph_ethad1   , xAOD::EgammaParameters::ShowerShapeType::ethad1   ); 
   ph->showerShapeValue( m_ph_ehad1    , xAOD::EgammaParameters::ShowerShapeType::ehad1    );
   ph->showerShapeValue( m_ph_f1       , xAOD::EgammaParameters::ShowerShapeType::f1       ); // LH
   ph->showerShapeValue( m_ph_f3       , xAOD::EgammaParameters::ShowerShapeType::f3       ); // LH
   ph->showerShapeValue( m_ph_f1core   , xAOD::EgammaParameters::ShowerShapeType::f1core   );
   ph->showerShapeValue( m_ph_f3core   , xAOD::EgammaParameters::ShowerShapeType::f3core   );
   ph->showerShapeValue( m_ph_weta1    , xAOD::EgammaParameters::ShowerShapeType::weta1    ); // LH (new)
   ph->showerShapeValue( m_ph_weta2    , xAOD::EgammaParameters::ShowerShapeType::weta2    ); // LH
   ph->showerShapeValue( m_ph_wtots1   , xAOD::EgammaParameters::ShowerShapeType::wtots1   );
   ph->showerShapeValue( m_ph_fracs1   , xAOD::EgammaParameters::ShowerShapeType::fracs1   );
   ph->showerShapeValue( m_ph_Reta     , xAOD::EgammaParameters::ShowerShapeType::Reta     ); // LH
   ph->showerShapeValue( m_ph_Rphi     , xAOD::EgammaParameters::ShowerShapeType::Rphi     ); // LH
   ph->showerShapeValue( m_ph_Eratio   , xAOD::EgammaParameters::ShowerShapeType::Eratio   ); // LH
   ph->showerShapeValue( m_ph_Rhad     , xAOD::EgammaParameters::ShowerShapeType::Rhad     ); // LH
   ph->showerShapeValue( m_ph_Rhad1    , xAOD::EgammaParameters::ShowerShapeType::Rhad1    ); // LH
   ph->showerShapeValue( m_ph_deltaE   , xAOD::EgammaParameters::ShowerShapeType::DeltaE   ); // LH
   ph->showerShapeValue( m_ph_e277     , xAOD::EgammaParameters::ShowerShapeType::e277     ); // LH
  //Combined track/Cluter information
 
  if(ph->caloCluster()){
	hasCalo=true;
	m_ph_calo_et                 = getCluster_et( ph );
  	m_ph_calo_eta                = getCluster_eta( ph );
  	m_ph_calo_phi                = getCluster_phi( ph );
  	m_ph_calo_etaBE2             = ph->caloCluster()->etaBE(2); // LH
  	m_ph_calo_e                  = ph->caloCluster()->e(); // LH
  }
  m_ph_nGoodVtx             = getNGoodVertex();
  m_ph_nPileupPrimaryVtx    = getNPVtx();

  m_ph_loose 	 	    = ApplyPhotonPid(ph, "Loose");
  m_ph_medium 		    = ApplyPhotonPid(ph, "Medium");
  m_ph_tight 		    = ApplyPhotonPid(ph, "Tight");

  ph->isolationValue(val,xAOD::Iso::etcone20);
  m_ph_etCone->push_back(val);
  ph->isolationValue(val,xAOD::Iso::etcone30);
  m_ph_etCone->push_back(val);
  ph->isolationValue(val,xAOD::Iso::etcone40);
  m_ph_etCone->push_back(val);
  ph->isolationValue(val,xAOD::Iso::ptcone20);
  m_ph_ptCone->push_back(val);
  ph->isolationValue(val,xAOD::Iso::ptcone30);
  m_ph_ptCone->push_back(val);
  ph->isolationValue(val,xAOD::Iso::ptcone40);
  m_ph_ptCone->push_back(val);
  ph->isolationValue(val,xAOD::Iso::ptvarcone20);
  m_ph_ptCone->push_back(val);
  ph->isolationValue(val,xAOD::Iso::ptvarcone30);
  m_ph_ptCone->push_back(val);
  ph->isolationValue(val,xAOD::Iso::ptvarcone40);
  m_ph_ptCone->push_back(val);
  m_ph_hasCalo = hasCalo;
  m_ph_hasTrack = hasTrack;
 if(m_doCaloRings){
    ATH_MSG_DEBUG("Fill Photon calo rings...");
    if(!TrigEgammaAnalysisBaseTool::getCaloRings( ph , *m_ph_ringsE)){
      ATH_MSG_WARNING("Could not attach the calorRings information.");
    }
 }
return true;

}
