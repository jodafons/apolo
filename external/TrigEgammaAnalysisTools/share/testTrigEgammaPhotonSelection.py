# from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConfig import getEventSelectionTool
from TrigEgammaAnalysisTools import TrigEgammaAnalysisToolsConf
from egammaRec.Factories import ToolFactory

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
from RecExConfig.RecFlags import rec
from RecExConfig.RecAlgsFlags import recAlgs
from AthenaCommon.AppMgr import ToolSvc
import os

inputFile ='mc16_13TeV/jf17/AOD.11188558._001393.pool.root.1'
jps.AthenaCommonFlags.FilesInput = [inputFile] 

# main jobOption
include ("RecExCommon/RecExCommon_topOptions.py")
MessageSvc.debugLimit = 20000000
MessageSvc.infoLimit  = 20000000
# TDT
from TrigDecisionTool.TrigDecisionToolConf import Trig__TrigDecisionTool
tdt = Trig__TrigDecisionTool( "TrigDecisionTool" )
tdt.TrigDecisionKey='xTrigDecision'
tdt.Navigation.ReadonlyHolders=True
ToolSvc+=tdt

try:
  include ("AthenaMonitoring/DataQualityInit_jobOptions.py")
except Exception:
  treatException("Could not load AthenaMonitoring/DataQualityInit_jobOptions.py")

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from AthenaMonitoring.AthenaMonitoringConf import AthenaMonManager
topSequence += AthenaMonManager( "HLTMonManager")
HLTMonManager = topSequence.HLTMonManager

################ Mon Tools #################
#Global HLTMonTool

from TrigHLTMonitoring.TrigHLTMonitoringConf import HLTMonTool
HLTMon = HLTMonTool(name  = 'HLTMon', histoPathBase = "HLT");

ToolSvc += HLTMon;
HLTMonManager.AthenaMonTools += [ "HLTMonTool/HLTMon" ];



from TrigEgammaAnalysisTools import TrigEgammaAnalysisToolsConfig
from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConfig import TrigEgammaPlotTool, getPhotonSelectionTool
TrigEgammaPhotonSelection = getPhotonSelectionTool()

supportList = [	#"HLT_g0_perf_L1EM3_EMPTY", #Use them for photon signal selection in MC data
		"HLT_g10_etcut",
#		"HLT_e10_etcut_L1EM7",
		#"HLT_g3_etcut_L1EM3_EMPTY",
		#"HLT_g15_loose",
		#"HLT_g0_perf_L1EM3",
		#"HLT_g0_perf_L1EM15"
		]

from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConfig import TrigEgammaPlotTool, getEventSelectionTool
#Define the base path for all histograms
basePath = '/HLT/Physval'

#Configure the TrigEgammaPlotTool
PlotTool = TrigEgammaPlotTool.copy( name="TrigEgammaPlotTool",
                                    DirectoryPath=basePath,
                                    #MaM=monitoring_mam,
                                    Efficiency=[],
                                    Distribution=[],
                                    Resolution=[])


Tool = TrigEgammaPhotonSelection(	name='TrigEgammaPhotonSelection',
					PlotTool = PlotTool,
				 	SelectionMC = True,
					FakeSelection = True,
		                  	SupportTriggerList=supportList)




Tools=['TrigEgammaPhotonSelection']

from TrigEgammaAnalysisTools.TrigEgammaAnalysisToolsConf import TrigEgammaMonTool
TrigEgammaMonTool = TrigEgammaMonTool( name = "HLTEgammaMon", 
                                       histoPathBase=basePath,
                                       Tools=Tools)

ToolSvc += TrigEgammaMonTool

#TrigEgammaMonToolConfig.TrigEgammaMonTool()
HLTMonManager.AthenaMonTools += [ "TrigEgammaMonTool/HLTEgammaMon" ]
HLTMonManager.FileKey = "GLOBAL"
