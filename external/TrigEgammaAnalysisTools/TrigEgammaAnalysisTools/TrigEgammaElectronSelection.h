#ifndef TRIGEGAMMAANALYSISTOOLS_TRIGEGAMMAELECTRONSELECTION_H
#define TRIGEGAMMAANALYSISTOOLS_TRIGEGAMMAELECTRONSELECTION_H

#include "TrigEgammaAnalysisTools/TrigEgammaNavTPBaseTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaPhysValBase.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/Vertex.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/EgammaEnums.h"
#include "xAODTracking/TrackingPrimitives.h"
#include <map>

#include "xAODTruth/xAODTruthHelpers.h"
#include "MCTruthClassifier/IMCTruthClassifier.h"
#include "MCTruthClassifier/MCTruthClassifierDefs.h"




class TrigEgammaElectronSelection : public TrigEgammaNavTPBaseTool, public TrigEgammaPhysValBase,
                          virtual public ITrigEgammaAnalysisBaseTool
{
  ASG_TOOL_CLASS(TrigEgammaElectronSelection, ITrigEgammaAnalysisBaseTool)

  public:
  
    TrigEgammaElectronSelection( const std::string& myname );
    ~TrigEgammaElectronSelection() {}; 
    StatusCode childInitialize();
    StatusCode childBook();
    StatusCode childExecute();
    StatusCode childFinalize();
 
  private:
    
    void Match( const xAOD::Electron* , const HLT::TriggerElement *&);
    
    /* Dump trigger and offline probe object */
    bool EventSelectionZ();
    bool EventSelectionJpsi();
    bool EventSelectionFakes();
   
    bool fill(TTree *t, const xAOD::Electron *el, const xAOD::Electron *el_tag=nullptr );

    /* parse between objects and local variables connected to the 
     * ttree pointer */
    bool fillEvent        ( );
    bool fillMonteCarlo   ( const xAOD::Electron      *el         );
    bool fillElectron     ( const xAOD::Electron      *el         );
    bool fillCaloRings    ( const xAOD::Electron      *el         );
    bool fillEmTauRoI     ( const xAOD::EmTauRoI      *emTauRoI   );
    bool fillTrigEMCluster( const xAOD::TrigEMCluster *emCluster  );
    bool fillTrigElectron ( const xAOD::TrigElectron  *trigEl     );
    bool fillCaloCluster  ( const xAOD::CaloCluster   *cluster    );
    bool fillAccept       ( const HLT::TriggerElement *           );
    bool fillTrigCaloRings( const HLT::TriggerElement *feat       );
    bool fillHLTElectron  ( const xAOD::Electron      *el         );
    bool fillTDT          ( const xAOD::Electron      *el         );
    bool fillTap          ( const xAOD::Electron *el_tag, const xAOD::Electron *el_probe );

    // Helper methods
    double get_el_sigd0(const xAOD::Electron *el);
    double get_el_d0(const xAOD::Electron *el);
    double get_el_eProbabilityHT(const xAOD::Electron *el);
    double get_el_transformed_eProbabilityHT(const xAOD::Electron *el);
    double get_el_d0significance(const xAOD::Electron *el);
    double get_el_deltaPhiRescaled2(const xAOD::Electron *el);
    double get_el_deltaEta1(const xAOD::Electron *el);
    double get_el_DeltaPOverP(const xAOD::Electron *el);


    bool isTruthElectronFromZ(const xAOD::Electron *);
    bool isTruthElectronFromW(const xAOD::Electron *);
    bool isTruthElectronFromJpsi(const xAOD::Electron *);
    bool isTruthElectronAny(const xAOD::Electron *);
    bool passTrackQuality(const xAOD::Electron *);
    bool ApplyFireTriggers( const xAOD::Electron *el);

  private:
    
    std::map<std::string, unsigned >m_countMap;

    void count( std::string key ){
      if(m_countMap.find(key) == m_countMap.end()){
        m_countMap[key]= 1;
      }else{
        m_countMap[key]++;
      }
    }

    void TriggerCounts(){
      std::vector<std::string> chains  = tdt()->getListOfTriggers("HLT_e.*, L1_EM.*, HLT_g.*");
      for (const auto &trigger : chains){
        if (tdt()->isPassed(trigger))
          count(trigger);
      }
    }

    bool WriteTriggerCounts(){  
      std::ofstream ofile("HLTTriggerCounts.txt");
      std::vector<std::string> chains  = tdt()->getListOfTriggers("HLT_e.*, L1_EM.*, HLT_g.*");
      if (!ofile.is_open()) return false;
      ofile << "=================> List of triggers <==================\n";
      for (const auto &trigger : chains) {
        ofile << trigger << " " << m_countMap[trigger] << "\n";
      }
      ofile.close();
      return true;
    }

    ToolHandleArray<IAsgElectronIsEMSelector>   m_EFCaloElectronIsEMSelectors;
    ToolHandleArray<IAsgElectronIsEMSelector>   m_HLTElectronIsEMSelectors;
    ToolHandleArray<IAsgElectronLikelihoodTool> m_EFCaloElectronLHSelectors;
    ToolHandleArray<IAsgElectronLikelihoodTool> m_HLTElectronLHSelectors;
    ToolHandle<IAsgElectronLikelihoodTool>      m_electronLHVLooseTool;
    ToolHandle<IMCTruthClassifier>              m_truthClassifier;



    int  m_detailedDataLevel; 
    bool m_selectionZ;
    bool m_selectionMC;
    bool m_selectionJpsi;
    bool m_selectionFakes;
    bool m_doCaloRings;

    std::vector<std::string> m_supportTrigList;
    std::vector<std::string> m_jpsiTriggers;
    bool  m_doJpsiTagTrackIsolation;
    bool  m_doJpsiTagTighterTRT;
    bool  m_doJpsiProbeTRTCut;
    bool  m_doJpsiProbeSiCut;
    float m_jpsiDeltaR;
    bool m_isBphys;
  
};



#endif
