/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/
#ifndef TRIGEGAMMAANALYSISTOOLS_TRIGEGAMMAPHOTONSELECTION_H
#define TRIGEGAMMAANALYSISTOOLS_TRIGEGAMMAPHOTONSELECTION_H

#include "TrigEgammaAnalysisTools/TrigEgammaNavTPBaseTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaPhysValBase.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/Vertex.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/EgammaEnums.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "TrigEgammaMatchingTool/TrigEgammaMatchingTool.h"
#include "xAODTrigRinger/versions/TrigRingerRings_v2.h"
#include "xAODTrigRinger/TrigRingerRingsContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTrigEgamma/TrigPhoton.h"
#include "xAODTrigEgamma/TrigPhotonContainer.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "xAODJet/JetContainer.h"
#include <map>

class TrigEgammaPhotonSelection : public TrigEgammaNavTPBaseTool, public TrigEgammaPhysValBase,
                          virtual public ITrigEgammaAnalysisBaseTool
{
  ASG_TOOL_CLASS(TrigEgammaPhotonSelection, ITrigEgammaAnalysisBaseTool)

  public:
  
    TrigEgammaPhotonSelection( const std::string& myname);
    ~TrigEgammaPhotonSelection();
   	StatusCode childInitialize ();
	  StatusCode childExecute ();
	  StatusCode childBook();

  
  protected:
    bool fillEmTauRoI( const xAOD::EmTauRoI *emTauRoI );
    bool fillTrigEMCluster( const xAOD::TrigEMCluster *emCluster );
    bool fillCaloCluster( const xAOD::CaloCluster *cluster);
    bool fillEvent();
    bool fillMonteCarlo(const xAOD::Photon *ph);
    bool fillTrigCaloRings( const HLT::TriggerElement *feat );
    bool Match( const xAOD::Egamma *eg, const HLT::TriggerElement *&feat);
    bool EventSelectionMC();
    bool EventSelectionReal();
    bool fill( TTree *t, const xAOD::Photon *ph);
    bool fillHLTPhoton( const xAOD::Photon *ph );
    bool fillTDT(const xAOD::Photon *ph );
    bool fillPhoton ( const xAOD::Photon *ph );	
    bool fillTrigPhoton( const xAOD::TrigPhoton *trigPh );


  
 
  private:
    std::map<std::string, unsigned >m_countMap;
    void count( std::string key ){
      if(m_countMap.find(key) == m_countMap.end()){
        m_countMap[key]= 1;
      }else{
        m_countMap[key]++;
      }
    }
	std::vector<std::string> 			m_supportList;
	bool 						m_selectionMC;
	bool						m_fakeSelection;
	bool 						m_doCaloRings;
	ToolHandleArray<IAsgPhotonIsEMSelector>   	m_EFCaloPhotonIsEMSelectors;
	ToolHandleArray<IAsgPhotonIsEMSelector>   	m_HLTPhotonIsEMSelectors;
	int 						m_detailedDataLevel;
    

};



#endif
