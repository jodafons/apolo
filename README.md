# Apolo

> DISCLAIMER: This framework only works using the Run 2 enviroments.

## How to setup and build it:

Use the `setup_here.sh` script to setup the athena release used during the end of
the run 2.

```
source setup_here.sh
```

And compile the code:

```
mkdir build && cd build
cmake .. && make -j4
cd ..
```

After compile the code you must set your modification to the system path using:

```
source build/x86_64-centos7-gcc62-opt/setup.sh
```




